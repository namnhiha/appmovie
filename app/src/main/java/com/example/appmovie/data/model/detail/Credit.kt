package com.example.appmovie.data.model.detail

import com.example.appmovie.data.model.detail.CastMovie

data class Credit(
    val id: Int,
    val cast: MutableList<CastMovie>
)
