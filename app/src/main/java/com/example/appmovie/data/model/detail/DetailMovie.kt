package com.example.appmovie.data.model.detail

import com.google.gson.annotations.SerializedName

data class DetailMovie(
    val id: Int,
    val genres: MutableList<Genres>,
    @SerializedName("poster_path")
    val posterPath: String,
    @SerializedName("release_date")
    val releaseDate: String,
    val title: String,
    @SerializedName("vote_average")
    val voteAverage: Float,
    @SerializedName("vote_count")
    val voteCount: Int,
    @SerializedName("overview")
    val overView: String,
    val popularity: Float
)
