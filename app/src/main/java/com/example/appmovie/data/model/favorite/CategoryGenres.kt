package com.example.appmovie.data.model.favorite

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "categories")
data class CategoryGenres(
    @PrimaryKey
    @ColumnInfo(name = "idCategory")
    var idCategory: Int,
    @ColumnInfo(name ="nameCategory")
    var nameCategory:String
)
