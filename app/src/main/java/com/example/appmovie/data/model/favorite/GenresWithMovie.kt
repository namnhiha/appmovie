package com.example.appmovie.data.model.favorite

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation

data class GenresWithMovie(
    @Embedded val genres: CategoryGenres,
    @Relation(
        parentColumn = "idCategory",
        entityColumn = "id",
        associateBy = Junction(MovieAndGenres::class, parentColumn = "idCategory", entityColumn = "id")
    )
    val playMovie: MutableList<MovieFavorite>
)
