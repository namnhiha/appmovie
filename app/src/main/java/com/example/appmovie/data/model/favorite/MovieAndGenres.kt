package com.example.appmovie.data.model.favorite

import androidx.room.Entity

@Entity(primaryKeys = ["id", "idCategory"])
data class MovieAndGenres(val id: Int, val idCategory: Int)
