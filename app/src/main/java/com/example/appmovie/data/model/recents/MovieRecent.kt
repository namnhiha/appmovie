package com.example.appmovie.data.model.recents

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "recents")
data class MovieRecent(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "autoid")
    var autoId: Int,
    @ColumnInfo(name = "id")
    var id: Int,
    @ColumnInfo(name = "name")
    val title: String,
    @ColumnInfo(name = "posterpath")
    val posterPath: String,
    @ColumnInfo(name = "releasedate")
    val releaseDate: String,
    @ColumnInfo(name = "voteaverage")
    var voteAverage: Float,
    @ColumnInfo(name = "votecount")
    val voteCount: Int,
    @ColumnInfo(name = "overview")
    val overView: String,
    @ColumnInfo(name = "checkfavorite")
    val checkFavorite: Boolean
)
