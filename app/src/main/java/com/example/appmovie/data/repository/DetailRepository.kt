package com.example.appmovie.data.repository

import androidx.paging.PagingData
import com.example.appmovie.data.model.Movie
import com.example.appmovie.data.model.detail.Credit
import com.example.appmovie.data.model.detail.DetailMovie
import com.example.appmovie.data.model.favorite.CategoryGenres
import com.example.appmovie.data.model.favorite.MovieFavorite
import com.example.appmovie.data.model.recents.MovieRecent
import com.example.appmovie.data.repository.local.DetailLocalDataSource
import com.example.appmovie.data.repository.remote.DetailRemoteDataSource
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

class DetailRepository @Inject constructor(
    private val detailLocalDataSource: DetailLocalDataSource,
    private val detailRemoteDataSource: DetailRemoteDataSource
) {
    //remote
    fun getCastMovie(id: Int): Single<Credit> {
        return detailRemoteDataSource.getCast(id)
    }
    fun getGenresMovie(id: Int): Single<DetailMovie> {
        return detailRemoteDataSource.getGenres(id)
    }

    fun getSimilarMovie(movieId: Int): Flowable<PagingData<Movie>> {
        return detailRemoteDataSource.getSimilarMovie(movieId)
    }

    fun getRecomendationMovie(movieId: Int): Flowable<PagingData<Movie>> {
        return detailRemoteDataSource.getRecomendationMovie(movieId)
    }
    //local

    fun createMovieFavorite(movieFavorite: MovieFavorite): Long {
        return detailLocalDataSource.createMovieFavorite(movieFavorite)
    }
    fun createCategoryGenres(categoryGenres: MutableList<CategoryGenres>,id:Int)
    {
        detailLocalDataSource.createCategoryGenres(categoryGenres,id)
    }

    fun deleteMovieFavorite(movieFavorite: MovieFavorite)
    {
        return detailLocalDataSource.deleteMovieFavorite(movieFavorite)
    }

    fun getMovieIdFavorite(id: Int): Single<MovieFavorite> {
        return detailLocalDataSource.getMovieIdFavorite(id)
    }

    fun createMovieRecent(movie: MovieRecent) {
        detailLocalDataSource.createMovieRecent(movie)
    }

    fun deleteAndCreateMovieRecent(movie:MovieRecent)
    {
        return detailLocalDataSource.deleteAndCreateMovieRecent(movie)
    }
}
