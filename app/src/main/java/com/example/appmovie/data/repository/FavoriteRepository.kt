package com.example.appmovie.data.repository

import com.example.appmovie.data.model.favorite.CategoryGenres
import com.example.appmovie.data.model.favorite.MovieFavorite
import com.example.appmovie.data.repository.local.FavoriteLocalDataSource
import com.example.appmovie.data.repository.remote.FavoriteRemoteDataSource
import io.reactivex.Flowable
import javax.inject.Inject

class FavoriteRepository @Inject constructor(
    private val favoriteLocalDataSource: FavoriteLocalDataSource,
    private val favoriteRemoteDataSource: FavoriteRemoteDataSource
) {
    fun getAllMovieFavorite(): Flowable<MutableList<MovieFavorite>> {
        return favoriteLocalDataSource.getMovieFavorite()
    }

    fun getAllCategoryGenres(): Flowable<MutableList<CategoryGenres>> {
        return favoriteLocalDataSource.getCategoryGenres()
    }

    fun deleteFavorite(movieFavorite: MovieFavorite) {
        return favoriteLocalDataSource.deleteFavorite(movieFavorite)
    }

    fun getMovieFavoriteGenres(list: MutableList<String>): Flowable<MutableList<MovieFavorite>> {
        return favoriteLocalDataSource.getMovieFavoriteGenres(list)
    }
}
