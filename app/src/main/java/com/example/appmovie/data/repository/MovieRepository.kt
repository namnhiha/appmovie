package com.example.appmovie.data.repository

import androidx.paging.PagingData
import com.example.appmovie.data.model.Movie
import com.example.appmovie.data.model.MovieResponse
import com.example.appmovie.data.model.recents.MovieRecent
import com.example.appmovie.data.repository.local.MovieLocalDataSource
import com.example.appmovie.data.repository.remote.MovieRemoteDataSource
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

class MovieRepository @Inject constructor(
    private val movieLocalDataSource: MovieLocalDataSource,
    private val movieRemoteDataSource: MovieRemoteDataSource
) {
    fun getMoive1(): Single<MovieResponse>
    {
        return movieRemoteDataSource.getMovie1()
    }
    fun getMovie(): Flowable<PagingData<Movie>> {
        return movieRemoteDataSource.getMovie()
    }
    fun getUpcomingMovie(): Flowable<PagingData<Movie>> {
        return movieRemoteDataSource.getUpcomingMovie()
    }

    fun getTopRatedMovie(): Flowable<PagingData<Movie>> {
        return movieRemoteDataSource.getTopRatedMovie()
    }

    fun getAllMovieRecent(): Flowable<MutableList<MovieRecent>> {
        return movieLocalDataSource.getMovieRecent()
    }

    fun getMovieZip(): Single<MovieResponse> {
        return movieRemoteDataSource.getMovieZip()
    }

    fun getUpcomingMovieZip(): Single<MovieResponse> {
        return movieRemoteDataSource.getUpcomingMovieZip()
    }

    fun getTopRatedMovieZip(): Single<MovieResponse> {
        return movieRemoteDataSource.getTopRatedMovieZip()
    }

    fun getMovieFavoriteId(id:Int):Single<Boolean>
    {
        return movieLocalDataSource.getMovieFavorite(id)
    }
}
