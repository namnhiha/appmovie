package com.example.appmovie.data.repository

import com.example.appmovie.data.model.recents.MovieRecent
import com.example.appmovie.data.repository.local.RecentSearchLocalDataSource
import com.example.appmovie.data.repository.remote.RecentSearchRemoteDataSource
import io.reactivex.Flowable
import javax.inject.Inject

class RecentSearchRepository @Inject constructor(
    private val recentSearchLocalDataSource: RecentSearchLocalDataSource,
    private val recentSearchRemoteDataSource: RecentSearchRemoteDataSource
) {
    fun getAllMovieRecent(): Flowable<MutableList<MovieRecent>> {
        return recentSearchLocalDataSource.getMovieRecent()
    }

    fun deleteAllMovieRecent() {
        recentSearchLocalDataSource.deleteAllMovieRecent()
    }
}
