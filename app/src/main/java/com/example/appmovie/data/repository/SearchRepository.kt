package com.example.appmovie.data.repository

import androidx.paging.PagingData
import com.example.appmovie.data.model.Movie
import com.example.appmovie.data.repository.local.SearchLocalDataSource
import com.example.appmovie.data.repository.remote.SearchRemoteDataSource
import io.reactivex.Flowable
import javax.inject.Inject

class SearchRepository @Inject constructor(
    private val searchLocalDataSource: SearchLocalDataSource,
    private val searchRemoteDataSource: SearchRemoteDataSource
) {
    fun searchMovie(query: String): Flowable<PagingData<Movie>> {
        return searchRemoteDataSource.searchMovie(query)
    }
}
