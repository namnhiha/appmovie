package com.example.appmovie.data.repository.local

import com.example.appmovie.data.model.favorite.CategoryGenres
import com.example.appmovie.data.model.favorite.MovieAndGenres
import com.example.appmovie.data.model.favorite.MovieFavorite
import com.example.appmovie.data.model.recents.MovieRecent
import com.example.appmovie.data.repository.local.database.DaoCategoryGenres
import com.example.appmovie.data.repository.local.database.DaoMovie
import com.example.appmovie.data.repository.local.database.DaoMovieRecent
import io.reactivex.Single
import javax.inject.Inject

class DetailLocalDataSource @Inject constructor(
    private val mDaoMovie: DaoMovie,
    private val mDaoMovieRecent: DaoMovieRecent,
    private val mDaoCategoryGenres: DaoCategoryGenres
) {

    fun createMovieFavorite(movie: MovieFavorite): Long {
        return mDaoMovie.insertMovie(movie)
    }

    fun createCategoryGenres(categoryGenres: MutableList<CategoryGenres>, id: Int) {
        mDaoCategoryGenres.insertCategoryGenres(categoryGenres)
        for (i in categoryGenres) {
            mDaoCategoryGenres.insertMovieAndGenresObject(MovieAndGenres(id, i.idCategory))
        }
    }

    fun deleteMovieFavorite(movie: MovieFavorite) {
        return mDaoMovie.deleteMovie(movie)
    }

    fun getMovieIdFavorite(id: Int): Single<MovieFavorite> {
        return mDaoMovie.getMovieById(id)
    }

    fun createMovieRecent(movie: MovieRecent) {
        mDaoMovieRecent.insertMovieRecent(movie)
    }

    fun deleteAndCreateMovieRecent(movie: MovieRecent) {
        return mDaoMovieRecent.deleteAndCreate(movie)
    }
}
