package com.example.appmovie.data.repository.local

import com.example.appmovie.data.model.favorite.CategoryGenres
import com.example.appmovie.data.model.favorite.MovieFavorite
import com.example.appmovie.data.repository.local.database.DaoCategoryGenres
import com.example.appmovie.data.repository.local.database.DaoMovie
import io.reactivex.Flowable
import javax.inject.Inject

class FavoriteLocalDataSource @Inject constructor(
    private val mDaoMovie: DaoMovie,
    private val mDaoCategoryGenres: DaoCategoryGenres
) {
    fun getMovieFavorite(): Flowable<MutableList<MovieFavorite>> {
        return mDaoMovie.getAllMovie()
    }

    fun getCategoryGenres(): Flowable<MutableList<CategoryGenres>> {
        return mDaoCategoryGenres.getAllCategory()
    }

    fun deleteFavorite(movieFavorite: MovieFavorite) {
        return mDaoMovie.deleteMovie(movieFavorite)
    }

    fun getMovieFavoriteGenres(list: MutableList<String>): Flowable<MutableList<MovieFavorite>> {
        return mDaoCategoryGenres.moviesQuery(list)
    }
}
