package com.example.appmovie.data.repository.local

import com.example.appmovie.data.model.recents.MovieRecent
import com.example.appmovie.data.repository.local.database.DaoMovie
import com.example.appmovie.data.repository.local.database.DaoMovieRecent
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

class MovieLocalDataSource @Inject constructor(private val mDaoMovieRecent: DaoMovieRecent,private val mDaoMovie: DaoMovie) {
    fun getMovieRecent(): Flowable<MutableList<MovieRecent>> {
        return mDaoMovieRecent.getAllMovieRecent()
    }

    fun getMovieFavorite(id: Int): Single<Boolean>
    {
        return mDaoMovie.isRowIsExist(id)
    }
}
