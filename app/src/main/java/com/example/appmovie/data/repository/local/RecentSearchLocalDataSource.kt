package com.example.appmovie.data.repository.local

import com.example.appmovie.data.model.recents.MovieRecent
import com.example.appmovie.data.repository.local.database.DaoMovieRecent
import io.reactivex.Flowable
import javax.inject.Inject

class RecentSearchLocalDataSource @Inject constructor(private val mDaoMovieRecent: DaoMovieRecent) {
    fun getMovieRecent(): Flowable<MutableList<MovieRecent>> {
        return mDaoMovieRecent.getAllMovieRecent()
    }

    fun deleteAllMovieRecent()
    {
        mDaoMovieRecent.deleteAllMovieRecent()
    }
}
