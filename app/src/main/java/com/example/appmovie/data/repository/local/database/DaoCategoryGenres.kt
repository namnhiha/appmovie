package com.example.appmovie.data.repository.local.database

import androidx.room.*
import com.example.appmovie.data.model.favorite.CategoryGenres
import com.example.appmovie.data.model.favorite.GenresWithMovie
import com.example.appmovie.data.model.favorite.MovieAndGenres
import com.example.appmovie.data.model.favorite.MovieFavorite
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface DaoCategoryGenres {

    @Query("select * from categories")
    fun getAllCategory(): Flowable<MutableList<CategoryGenres>>

    @Query("select * from categories where idCategory in (:id)")
    fun getCategoryGenresById(id: Int): Single<CategoryGenres>

    @Query("delete from categories")
    fun deleteAllCategoryGenres()

    @Query("delete from categories where idCategory=(:id)")
    fun deleteMovieRecent(id: Int)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCategoryGenres(categoryGenres: CategoryGenres)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCategoryGenres(categoryGenres: MutableList<CategoryGenres>)

    @Update
    fun updateMovieRecent(categoryGenres: CategoryGenres)

    @Delete
    fun deleteMovieRecent(categoryGenres: CategoryGenres)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMovieAndGenres(movieAndGenres: MutableList<MovieAndGenres>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMovieAndGenresObject(movieAndGenres: MovieAndGenres)

    @Transaction
    @Query("SELECT * FROM categories")
    fun getGenresWithListMovie(): List<GenresWithMovie>

    @Transaction
    @Query(
        """
        SELECT * FROM movies
        JOIN MovieAndGenres ON MovieAndGenres.id=movies.id
        JOIN categories ON MovieAndGenres.idCategory = categories.idCategory
        WHERE categories.nameCategory IN (:categoryName)
    """
    )
    fun moviesQuery(categoryName: MutableList<String>):Flowable<MutableList<MovieFavorite>>
}
