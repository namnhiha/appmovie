package com.example.appmovie.data.repository.local.database

import androidx.room.*
import com.example.appmovie.data.model.favorite.MovieFavorite
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface DaoMovie {
    @Query("select * from movies")
    fun getAllMovie(): Flowable<MutableList<MovieFavorite>>

    @Query("select * from movies where id in (:id)")
    fun getMovieById(id: Int): Single<MovieFavorite>

    @Query("SELECT EXISTS(SELECT * FROM movies WHERE id = :id)")
    fun isRowIsExist(id: Int): Single<Boolean>

    @Query("delete from movies")
    fun deleteAllMovie()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMovie(movie: MovieFavorite): Long

    @Update
    fun updateMovie(movie: MovieFavorite)

    @Delete
    fun deleteMovie(movie: MovieFavorite)
}
