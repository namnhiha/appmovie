package com.example.appmovie.data.repository.local.database

import androidx.room.*
import com.example.appmovie.data.model.recents.MovieRecent
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface DaoMovieRecent {
    @Query("select * from recents order by autoid desc")
    fun getAllMovieRecent(): Flowable<MutableList<MovieRecent>>

    @Query("select * from recents where id in (:id)")
    fun getMovieRecentById(id: Int): Single<MovieRecent>

    @Query("delete from recents")
    fun deleteAllMovieRecent()

    @Query("delete from recents where id=(:id)")
    fun deleteMovieRecent(id: Int)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMovieRecent(movie: MovieRecent)

    @Update
    fun updateMovieRecent(movie: MovieRecent)

    @Delete
    fun deleteMovieRecent(movie: MovieRecent)

    @Transaction
    fun deleteAndCreate(movieRecent:MovieRecent) {
        deleteMovieRecent(movieRecent.id)
        insertMovieRecent(movieRecent)
    }
}
