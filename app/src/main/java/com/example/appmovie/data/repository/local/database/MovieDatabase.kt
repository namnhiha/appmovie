package com.example.appmovie.data.repository.local.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.appmovie.data.model.favorite.CategoryGenres
import com.example.appmovie.data.model.favorite.MovieAndGenres
import com.example.appmovie.data.model.favorite.MovieFavorite
import com.example.appmovie.data.model.recents.MovieRecent

@Database(
    entities = [MovieFavorite::class, MovieRecent::class, CategoryGenres::class,MovieAndGenres::class],
    version = 1,
    exportSchema = false
)
abstract class MovieDatabase : RoomDatabase() {

    abstract fun movieDao(): DaoMovie
    abstract fun movieDaoRecent(): DaoMovieRecent
    abstract fun movieDaoCategorieGenres(): DaoCategoryGenres

    companion object {

        @Volatile
        private var instance: MovieDatabase? = null

        fun getInstance(context: Context): MovieDatabase {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also { instance = it }
            }
        }

        private fun buildDatabase(context: Context): MovieDatabase {
            return Room.databaseBuilder(context, MovieDatabase::class.java, "moviedb")
                .build()
        }
    }
}
