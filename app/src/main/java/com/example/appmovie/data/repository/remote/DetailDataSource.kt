package com.example.appmovie.data.repository.remote

import androidx.paging.rxjava2.RxPagingSource
import com.example.appmovie.data.model.Movie
import com.example.appmovie.data.model.MovieResponse
import com.example.appmovie.data.repository.remote.api.TheMovieDBInterface
import com.example.appmovie.utils.*
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class DetailDataSource(
    private var mService: TheMovieDBInterface,
    private var mAction: Int,
    private var mMovieId:Int
) : RxPagingSource<Int, Movie>() {

    override fun loadSingle(params: LoadParams<Int>): Single<LoadResult<Int, Movie>> {
        val position = params.key ?: 1
        return when (mAction) {
            ID_SIMILAR_MOVIE -> {
                mService.getSimilarMovie(mMovieId,position)
                    .subscribeOn(Schedulers.io())
                    .map {
                        toLoadResult(it, position)
                    }
                    .onErrorReturn {
                        LoadResult.Error(it)
                    }
            }
            ID_RECOMENDATION_MOVIE -> {
                mService.getRecomendationMovie(mMovieId,position)
                    .subscribeOn(Schedulers.io())
                    .map {
                        toLoadResult(it, position)
                    }
                    .onErrorReturn {
                        LoadResult.Error(it)
                    }
            }
            else -> {
                mService.getSimilarMovie(mMovieId,position)
                    .subscribeOn(Schedulers.io())
                    .map {
                        toLoadResult(it, position)
                    }
                    .onErrorReturn {
                        LoadResult.Error(it)
                    }
            }
        }
    }

    private fun toLoadResult(data: MovieResponse, position: Int): LoadResult<Int, Movie> {
        return LoadResult.Page(
            data = data.movieList,
            prevKey = if (position == 1) null else position - 1,
            nextKey = if (position == data.totalPages) null else position + 1
        )
    }
}
