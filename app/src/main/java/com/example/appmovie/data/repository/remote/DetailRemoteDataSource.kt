package com.example.appmovie.data.repository.remote

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.rxjava2.flowable
import com.example.appmovie.data.model.Movie
import com.example.appmovie.data.model.detail.Credit
import com.example.appmovie.data.model.detail.DetailMovie
import com.example.appmovie.data.repository.remote.api.TheMovieDBInterface
import com.example.appmovie.utils.ID_RECOMENDATION_MOVIE
import com.example.appmovie.utils.ID_SIMILAR_MOVIE
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

class DetailRemoteDataSource @Inject constructor(private val apiService: TheMovieDBInterface) {
    fun getCast(id: Int): Single<Credit> {
        return apiService.getCastMovie(id)
    }

    fun getGenres(id: Int): Single<DetailMovie> {
        return apiService.getDetailMovie(id)
    }

    fun getSimilarMovie(movieId: Int): Flowable<PagingData<Movie>> {
        return createFlowable(ID_SIMILAR_MOVIE, movieId)
    }

    fun getRecomendationMovie(movieId: Int): Flowable<PagingData<Movie>> {
        return createFlowable(ID_RECOMENDATION_MOVIE, movieId)
    }

    private fun createFlowable(action: Int, movieId: Int): Flowable<PagingData<Movie>> {
        return Pager(
            config = PagingConfig(
                pageSize = 20,
                enablePlaceholders = true,
                maxSize = 30,
                prefetchDistance = 5,
                initialLoadSize =2
            ),
            pagingSourceFactory = { DetailDataSource(apiService, action, movieId) }
        ).flowable
    }
}
