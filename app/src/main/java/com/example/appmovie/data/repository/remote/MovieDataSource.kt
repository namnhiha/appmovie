import android.util.Log
import androidx.paging.rxjava2.RxPagingSource
import com.example.appmovie.data.model.Movie
import com.example.appmovie.data.model.MovieResponse
import com.example.appmovie.data.repository.remote.api.TheMovieDBInterface
import com.example.appmovie.utils.ID_POPULAR
import com.example.appmovie.utils.ID_TOP_RATED
import com.example.appmovie.utils.ID_UP_COMMING
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class MovieDataSource(
    private var service: TheMovieDBInterface,
    private var action: Int
) : RxPagingSource<Int, Movie>() {

    override fun loadSingle(params: LoadParams<Int>): Single<LoadResult<Int, Movie>> {
        val position = params.key ?: 1
        return when (action) {
            ID_POPULAR -> {
                service.getPopularMovie(position)
                    .subscribeOn(Schedulers.io())
                    .map {
                        toLoadResult(it, position)
                    }
                    .onErrorReturn {
                        LoadResult.Error(it)
                    }
            }
            ID_UP_COMMING -> {
                service.getUpcomingMovie(position)
                    .subscribeOn(Schedulers.io())
                    .map {
                        toLoadResult(it, position)
                    }
                    .onErrorReturn {
                        LoadResult.Error(it)
                    }
            }
            ID_TOP_RATED -> {
                Log.i("TAG", "ALO")
                service.getTopRatedMovie(position)
                    .subscribeOn(Schedulers.io())
                    .map {
                        toLoadResult(it, position)
                    }
                    .onErrorReturn {
                        LoadResult.Error(it)
                    }
            }
            else -> {
                service.getPopularMovie(position)
                    .subscribeOn(Schedulers.io())
                    .map {
                        toLoadResult(it, position)
                    }
                    .onErrorReturn {
                        LoadResult.Error(it)
                    }
            }
        }
    }

    private fun toLoadResult(data: MovieResponse, position: Int): LoadResult<Int, Movie> {
        return LoadResult.Page(
            data = data.movieList,
            prevKey = if (position == 1) null else position - 1,
            nextKey = if (position == data.totalPages) null else position + 1
        )
    }
}
