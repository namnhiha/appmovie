package com.example.appmovie.data.repository.remote

import MovieDataSource
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.rxjava2.flowable
import com.example.appmovie.data.model.Movie
import com.example.appmovie.data.model.MovieResponse
import com.example.appmovie.data.repository.remote.api.TheMovieDBInterface
import com.example.appmovie.utils.ID_POPULAR
import com.example.appmovie.utils.ID_TOP_RATED
import com.example.appmovie.utils.ID_UP_COMMING
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

class MovieRemoteDataSource @Inject constructor(
    private val apiService: TheMovieDBInterface,
) {
    fun getMovie1():Single<MovieResponse>
    {
        return apiService.getPopularMovie(1)
    }

    fun getMovie(): Flowable<PagingData<Movie>> {
        return createFlowable(ID_POPULAR)
    }

    fun getUpcomingMovie(): Flowable<PagingData<Movie>> {
        return createFlowable(ID_UP_COMMING)
    }

    fun getTopRatedMovie(): Flowable<PagingData<Movie>> {
        return createFlowable(ID_TOP_RATED)
    }

    fun getMovieZip(): Single<MovieResponse> {
        return apiService.getPopularMovie(1)
    }

    fun getUpcomingMovieZip(): Single<MovieResponse> {
        return apiService.getUpcomingMovie(1)
    }

    fun getTopRatedMovieZip(): Single<MovieResponse> {
        return apiService.getTopRatedMovie(1)
    }

    private fun createFlowable(action: Int): Flowable<PagingData<Movie>> {
        return Pager(
            config = PagingConfig(
                pageSize = 20,
                enablePlaceholders = true,
                maxSize = 30,
                prefetchDistance = 5,
                initialLoadSize = 40
            ),
            pagingSourceFactory = { MovieDataSource(apiService, action) }
        ).flowable
    }
}
