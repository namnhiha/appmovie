package com.example.appmovie.data.repository.remote

import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.rxjava2.RxPagingSource
import com.example.appmovie.data.model.Movie
import com.example.appmovie.data.model.MovieResponse
import com.example.appmovie.data.repository.remote.api.TheMovieDBInterface
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class SearchMovieDataSource(
    private var service: TheMovieDBInterface,
    private var query: String
) : RxPagingSource<Int, Movie>() {
    override fun loadSingle(params: LoadParams<Int>): Single<LoadResult<Int, Movie>> {
        Log.i("TAG","XIN CHAO XIN CHAO")
        val position = params.key ?: 1
        return service.searchMovie(position, query)
            .subscribeOn(Schedulers.io())
            .map {
                toLoadResult(it, position)
            }
            .onErrorReturn {
                LoadResult.Error(it)
            }
    }

    private fun toLoadResult(
        data: MovieResponse,
        position: Int
    ): LoadResult<Int, Movie> {
        return LoadResult.Page(
            data = data.movieList,
            prevKey = if (position == 1) null else position - 1,
            nextKey = if (position == data.totalPages) null else position + 1
        )
    }
}
