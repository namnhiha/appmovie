package com.example.appmovie.data.repository.remote

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.rxjava2.flowable
import com.example.appmovie.data.model.Movie
import com.example.appmovie.data.model.MovieResponse
import com.example.appmovie.data.repository.remote.api.TheMovieDBInterface
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

class SearchRemoteDataSource @Inject constructor(private val apiService: TheMovieDBInterface) {
    fun searchMovie(query:String):Flowable<PagingData<Movie>> {
        return  return Pager(
            config = PagingConfig(
                pageSize = 20,
                enablePlaceholders = true,
                maxSize = 30,
                prefetchDistance = 5,
                initialLoadSize = 40
            ),
            pagingSourceFactory = { SearchMovieDataSource(apiService,query) }
        ).flowable
    }
}
