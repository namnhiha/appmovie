package com.example.appmovie.data.repository.remote

import androidx.paging.PagingSource
import com.example.appmovie.data.model.MovieResponse
import com.example.appmovie.data.repository.remote.api.TheMovieDBInterface
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class SliderDataSource(private var apiService: TheMovieDBInterface) {
    fun getMovie(): Single<MovieResponse> {
        return apiService.getPopularMovie(1)
            .subscribeOn(Schedulers.io())
    }
}
