package com.example.appmovie.data.repository.remote.api

import com.example.appmovie.data.model.detail.Credit
import com.example.appmovie.data.model.MovieResponse
import com.example.appmovie.data.model.detail.DetailMovie
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TheMovieDBInterface {

    //https://api.themoviedb.org/3/movie/popular?api_key=1cf151ff23247b3fe3558107667a395f&language=en-US&page=1
    @GET("movie/popular")
    fun getPopularMovie(@Query("page") page: Int): Single<MovieResponse>

    @GET("movie/upcoming")
    fun getUpcomingMovie(@Query("page") page: Int): Single<MovieResponse>

    @GET("movie/top_rated")
    fun getTopRatedMovie(@Query("page") page: Int): Single<MovieResponse>

    @GET("search/movie")
    fun searchMovie(@Query("page") page: Int, @Query("query") query: String): Single<MovieResponse>

    @GET("movie/{movie_id}/credits")
    fun getCastMovie(@Path("movie_id") movieId: Int): Single<Credit>

    @GET("movie/{movie_id}")
    fun getDetailMovie(@Path("movie_id") movieId: Int): Single<DetailMovie>

    @GET("movie/{movie_id}/similar")
    fun getSimilarMovie(
        @Path("movie_id") movied: Int,
        @Query("page") page: Int
    ): Single<MovieResponse>

    @GET("movie/{movie_id}/recommendations")
    fun getRecomendationMovie(
        @Path("movie_id") movied: Int,
        @Query("page") page: Int
    ): Single<MovieResponse>
}
