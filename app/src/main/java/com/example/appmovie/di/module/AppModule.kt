package com.example.appmovie.di.module

import android.app.Application
import com.example.appmovie.data.repository.local.database.MovieDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(
    includes = [
        ViewModelModule::class,
        NetworkModule::class,
        RepositoryModule::class
    ]
)
class AppModule {
//    @Provides
//    @Singleton
//    fun provideContext(app:BaseApplication): Context = app

//    @Provides
//    @Singleton
//    fun provideApplication(app:BaseApplication): Application = app

    @Provides
    @Singleton
    fun provideRoomDB(app: Application) = MovieDatabase.getInstance(app)

    @Provides
    @Singleton
    fun providePlayerDao(db: MovieDatabase) = db.movieDao()

    @Provides
    @Singleton
    fun providePlayerDaoRecent(db: MovieDatabase) = db.movieDaoRecent()

    @Provides
    @Singleton
    fun providePlayerDaoCategories(db: MovieDatabase) =db.movieDaoCategorieGenres()
}
