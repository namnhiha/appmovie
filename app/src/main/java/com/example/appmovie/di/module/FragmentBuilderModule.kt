package com.example.appmovie.di.module

import com.example.appmovie.ui.movie.detailmovie.DetailMovieFragment
import com.example.appmovie.ui.movie.favoritemovie.FavoriteMovieFragment
import com.example.appmovie.ui.movie.listmovie.ListMovieFragment
import com.example.appmovie.ui.movie.morelistmovie.MoreMovieFragment
import com.example.appmovie.ui.movie.root.RootFragment
import com.example.appmovie.ui.movie.searchmovie.SearchMovieFragment
import com.example.appmovie.ui.movie.searchmovie.listsearch.ListSearchFragment
import com.example.appmovie.ui.movie.searchmovie.recent.RecentSearchFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilderModule {
    @ContributesAndroidInjector
    abstract fun contributeRootFragment(): RootFragment

    @ContributesAndroidInjector
    abstract fun contributeListMovie(): ListMovieFragment

    @ContributesAndroidInjector
    abstract fun contributeSearchMovie(): SearchMovieFragment

    @ContributesAndroidInjector
    abstract fun contributeRecentSearchMovie(): RecentSearchFragment

    @ContributesAndroidInjector
    abstract fun contributeListSearchMovie(): ListSearchFragment

    @ContributesAndroidInjector
    abstract fun contributeDetailMovie(): DetailMovieFragment

    @ContributesAndroidInjector
    abstract fun contributeFavoriteMovie(): FavoriteMovieFragment

    @ContributesAndroidInjector
    abstract fun contributeMoreMovie(): MoreMovieFragment
}
