package com.example.appmovie.di.module

import com.example.appmovie.data.repository.DetailRepository
import com.example.appmovie.data.repository.MovieRepository
import com.example.appmovie.data.repository.RecentSearchRepository
import com.example.appmovie.data.repository.SearchRepository
import com.example.appmovie.data.repository.local.*
import com.example.appmovie.data.repository.local.database.DaoCategoryGenres
import com.example.appmovie.data.repository.local.database.DaoMovie
import com.example.appmovie.data.repository.local.database.DaoMovieRecent
import com.example.appmovie.data.repository.remote.DetailRemoteDataSource
import com.example.appmovie.data.repository.remote.MovieRemoteDataSource
import com.example.appmovie.data.repository.remote.RecentSearchRemoteDataSource
import com.example.appmovie.data.repository.remote.SearchRemoteDataSource
import com.example.appmovie.data.repository.remote.api.TheMovieDBInterface
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {
    @Singleton
    @Provides
    fun provideMovieRepository(
        movieLocalDataSource: MovieLocalDataSource, movieRemoteDataSource: MovieRemoteDataSource
    ): MovieRepository {
        return MovieRepository(movieLocalDataSource, movieRemoteDataSource)
    }

    @Singleton
    @Provides
    fun providesMovieRemoteDataSource(
        dbInterface: TheMovieDBInterface
    ): MovieRemoteDataSource =
        MovieRemoteDataSource(dbInterface)

    @Singleton
    @Provides
    fun providesMovieLocalDataSource(
        mDaoMovieRecent: DaoMovieRecent,
        mDaoMovie: DaoMovie
    ): MovieLocalDataSource = MovieLocalDataSource(mDaoMovieRecent,mDaoMovie)

    @Singleton
    @Provides
    fun provideSearchRepository(
        searchLocalDataSource: SearchLocalDataSource, searchRemoteDataSource: SearchRemoteDataSource
    ): SearchRepository {
        return SearchRepository(searchLocalDataSource, searchRemoteDataSource)
    }

    @Singleton
    @Provides
    fun providesSearchRemoteDataSource(
        dbInterface: TheMovieDBInterface
    ): SearchRemoteDataSource = SearchRemoteDataSource(dbInterface)

    @Singleton
    @Provides
    fun provideRecentSearchRepository(
        recentSearchLocalDataSource: RecentSearchLocalDataSource,
        recentSearchRemoteDataSource: RecentSearchRemoteDataSource
    ): RecentSearchRepository {
        return RecentSearchRepository(recentSearchLocalDataSource, recentSearchRemoteDataSource)
    }

    @Singleton
    @Provides
    fun providesRecentSearchLocalDataSource(
        mDaoMovieRecent: DaoMovieRecent
    ): RecentSearchLocalDataSource = RecentSearchLocalDataSource(mDaoMovieRecent)

    @Singleton
    @Provides
    fun provideDetailMovieRepository(
        movieDetailLocalDataSource: DetailLocalDataSource,
        movieDetailRemoteDataSource: DetailRemoteDataSource
    ): DetailRepository {
        return DetailRepository(movieDetailLocalDataSource, movieDetailRemoteDataSource)
    }

    @Singleton
    @Provides
    fun provideDetailRemoteDataSource(
        dbInterface: TheMovieDBInterface
    ): DetailRemoteDataSource = DetailRemoteDataSource(dbInterface)

    @Singleton
    @Provides
    fun provideDetailLocalDataSource(
        mDaoMovie: DaoMovie,
        mDaoMovieRecent: DaoMovieRecent,
        mDaoCategoryGenres: DaoCategoryGenres
    ): DetailLocalDataSource = DetailLocalDataSource(mDaoMovie, mDaoMovieRecent, mDaoCategoryGenres)

    @Singleton
    @Provides
    fun provideFavoriteLocalDataSource(
        mDaoMovie: DaoMovie,
        mDaoCategoryGenres: DaoCategoryGenres
    ): FavoriteLocalDataSource = FavoriteLocalDataSource(mDaoMovie, mDaoCategoryGenres)
}
