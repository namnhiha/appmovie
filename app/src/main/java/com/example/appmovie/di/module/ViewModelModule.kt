package com.example.appmovie.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.appmovie.ui.movie.detailmovie.DetailMovieViewModel
import com.example.appmovie.ui.movie.favoritemovie.FavoriteMovieViewModel
import com.example.appmovie.ui.movie.listmovie.ListMovieViewModel
import com.example.appmovie.ui.movie.morelistmovie.MoreMovieViewModel
import com.example.appmovie.ui.movie.searchmovie.listsearch.ListSearchMovieViewModel
import com.example.appmovie.ui.movie.searchmovie.recent.RecentSearchViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import javax.inject.Inject

@Module
abstract class ViewModelModule {
    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(ListMovieViewModel::class)
    abstract fun bindListMovieViewModel(ListMovieViewModel: ListMovieViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ListSearchMovieViewModel::class)
    abstract fun bindSearchMovieViewModel(ListSearchViewModel: ListSearchMovieViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DetailMovieViewModel::class)
    abstract fun bindDetailMovieViewModel(DetailViewModel: DetailMovieViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FavoriteMovieViewModel::class)
    abstract fun bindFavoriteMovieViewModel(FavoriteViewModel: FavoriteMovieViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RecentSearchViewModel::class)
    abstract fun bindRecentSearchMovieViewModel(RecentSearchViewModel: RecentSearchViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MoreMovieViewModel::class)
    abstract fun bindMoreMovieViewModel(MoreViewModel: MoreMovieViewModel): ViewModel
}
