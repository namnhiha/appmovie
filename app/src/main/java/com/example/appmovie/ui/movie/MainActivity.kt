package com.example.appmovie.ui.movie

import android.os.Bundle
import com.example.appmovie.R
import com.example.appmovie.ui.movie.root.RootFragment
import dagger.android.support.DaggerAppCompatActivity

class MainActivity : DaggerAppCompatActivity(R.layout.activity_main) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportFragmentManager.beginTransaction()
            .add(R.id.fragmentContainer, RootFragment.newInstance())
            .commit()
    }

    override fun onBackPressed() {
        try {
            var handled = false // 1
            supportFragmentManager.fragments.forEach { fragment ->
                if (fragment is RootFragment) {
                    handled = fragment.onBackPressed()
                }
            }
            if (!handled) {
                super.onBackPressed()
            }
        } catch (e: Exception) {
            super.onBackPressed()
        }
    }
}
