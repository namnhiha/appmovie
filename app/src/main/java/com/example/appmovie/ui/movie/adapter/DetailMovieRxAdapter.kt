package com.example.appmovie.ui.movie.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.appmovie.R
import com.example.appmovie.data.model.Movie
import com.example.appmovie.ui.movie.listmovie.ItemClick
import com.example.appmovie.utils.Config
import kotlinx.android.synthetic.main.movie_horizontal_item.view.*

class DetailMoviesRxAdapter : PagingDataAdapter<Movie, DetailMoviesRxAdapter.MovieGridViewHolder>(
    COMPARATOR
) {
    private var itemClick: ItemClick<Movie>? = null

    fun onClick(itemClick: ItemClick<Movie>) {
        this.itemClick = itemClick
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieGridViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.movie_horizontal_item, parent, false)
        return MovieGridViewHolder(view)
    }

    override fun onBindViewHolder(holder: MovieGridViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it)
        }
    }

    inner class MovieGridViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        @SuppressLint("SetTextI18n")
        fun bind(movie: Movie) {
            val moviePosterURL = Config.POSTER_BASE_URL + movie.posterPath
            itemView.apply {
                textViewNameMovie.text = movie.title.toString()
                Glide.with(this).load(moviePosterURL).into(imageViewPoster)
                textViewVoteAverage.text = movie.voteAverage.toString()
                textViewVoteCount.text = "(${movie.voteCount})"
            }
            itemView.setOnClickListener {
                itemClick?.clickItem(movie)
            }
        }
    }

    companion object {
        private val COMPARATOR = object : DiffUtil.ItemCallback<Movie>() {
            override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean {
                return oldItem.id == newItem.id
            }
        }
    }
}
