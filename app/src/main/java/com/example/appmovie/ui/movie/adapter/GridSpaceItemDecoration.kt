package com.example.appmovie.ui.movie.adapter

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class GridSpaceItemDecoration(private var horizontalSpaceHeight: Int) :
    RecyclerView.ItemDecoration() {
    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        outRect.right = horizontalSpaceHeight
        outRect.bottom=horizontalSpaceHeight
        outRect.left = horizontalSpaceHeight
        outRect.top=horizontalSpaceHeight
    }
}
