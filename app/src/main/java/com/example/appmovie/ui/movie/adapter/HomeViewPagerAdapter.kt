package com.example.appmovie.ui.movie.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.appmovie.ui.movie.favoritemovie.FavoriteMovieFragment
import com.example.appmovie.ui.movie.listmovie.ListMovieFragment
import com.example.appmovie.ui.movie.searchmovie.SearchMovieFragment

class HomeViewPagerAdapter(context: FragmentActivity) : FragmentStateAdapter(context) {

    override fun getItemCount() = NUMBER_COUNT

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> ListMovieFragment.newInstance()
            1 -> FavoriteMovieFragment.newInstance()
            2 -> SearchMovieFragment.newInstance()
            else -> ListMovieFragment.newInstance()
        }
    }

    companion object {
        const val NUMBER_COUNT = 3
    }
}
