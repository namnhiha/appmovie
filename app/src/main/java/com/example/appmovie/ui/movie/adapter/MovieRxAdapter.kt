package com.example.appmovie.ui.movie.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.appmovie.R
import com.example.appmovie.data.model.Movie
import com.example.appmovie.ui.movie.listmovie.ItemClick
import com.example.appmovie.utils.Config.POSTER_BASE_URL
import kotlinx.android.synthetic.main.item_vertical_load_more.view.*

class MoviesRxAdapter : PagingDataAdapter<Movie, MoviesRxAdapter.MovieGridViewHolder>(
    COMPARATOR
) {
    private var itemClick: ItemClick<Movie>? = null

    fun onClick(itemClick: ItemClick<Movie>) {
        this.itemClick = itemClick
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieGridViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_vertical_load_more, parent, false)
        return MovieGridViewHolder(view)
    }

    override fun onBindViewHolder(holder: MovieGridViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it)
        }
    }

    inner class MovieGridViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        @SuppressLint("SetTextI18n")
        fun bind(movie: Movie) {
            val moviePosterURL = POSTER_BASE_URL + movie.posterPath
            itemView.apply {
                textViewNameMovie.text = movie.title
                textViewOverView.text = movie.overView
                Glide.with(this).load(moviePosterURL).into(imageViewPoster)
                ratingBarMovieFavorite.rating = movie.voteAverage
            }
            itemView.setOnClickListener {
                itemClick?.clickItem(movie)
            }
        }
    }

    companion object {
        private val COMPARATOR = object : DiffUtil.ItemCallback<Movie>() {
            override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean {
                return oldItem.id == newItem.id
            }
        }
    }
}
