package com.example.appmovie.ui.movie.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.appmovie.R
import com.example.appmovie.data.model.Movie
import com.example.appmovie.data.model.ParentMovie
import com.example.appmovie.ui.movie.listmovie.ItemClick
import com.example.appmovie.ui.movie.listmovie.ItemClickLoadMore
import com.example.appmovie.ui.movie.listmovie.ListMovieFragment.Companion.mViewModelMovie
import com.example.appmovie.ui.movie.listmovie.adapter.MovieAdapter
import com.example.appmovie.ui.movie.listmovie.adapter.StartSnapHelper
import com.example.appmovie.utils.ID_POPULAR
import com.example.appmovie.utils.ID_RECENT_VIEWED
import com.example.appmovie.utils.ID_TOP_RATED
import com.example.appmovie.utils.ID_UP_COMMING
import kotlinx.android.synthetic.main.recyclerview_item.view.*
import java.util.*

class ParentMovieRxAdapter : ListAdapter<ParentMovie, RecyclerView.ViewHolder>(
    COMPARATOR
) {
    private var mListParentMovie = arrayListOf<ParentMovie>()
    private var mContext: Context? = null
    private var mLifecycle: Lifecycle? = null
    private var mItemClick: ItemClick<Movie>? = null
    private var mItemClickLoadMore: ItemClickLoadMore? = null
    private var mViewLifecycleOwner: LifecycleOwner? = null

    @SuppressLint("NotifyDataSetChanged")
    fun setData(
        list: ArrayList<ParentMovie>,
        context: Context,
        lifecycle: Lifecycle,
        lifecycleOwner: LifecycleOwner
    ) {
        mListParentMovie = list
        mContext = context
        mLifecycle = lifecycle
        mViewLifecycleOwner = lifecycleOwner
        notifyDataSetChanged()
    }

    fun setItemClick(item: ItemClick<Movie>) {
        this.mItemClick = item
    }

    fun setItemClickLoadMore(item: ItemClickLoadMore) {
        this.mItemClickLoadMore = item
    }

    override fun getItemCount() = mListParentMovie.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.recyclerview_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemViewType(position: Int): Int {
        return mListParentMovie[position].id
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (ID_RECENT_VIEWED == holder.itemViewType) {
            settingAdapterChild(holder as ViewHolder, ID_RECENT_VIEWED, position)
            (holder as ViewHolder).bind(mListParentMovie[position])
            val movieRecentAdapter = RecentAdapter()
            holder.itemView.run {
                val linearLayoutManager =
                    LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false)
                recyclerViewMovieItem.layoutManager = linearLayoutManager
                val horizontalSpaceItemDecoration = HorizontalSpaceItemDecoration(20)
                recyclerViewMovieItem.addItemDecoration(horizontalSpaceItemDecoration)
                recyclerViewMovieItem.adapter = movieRecentAdapter
            }
            mViewLifecycleOwner?.let {
                mViewModelMovie.getMovieRecent().observe(
                    it,
                    androidx.lifecycle.Observer { it1 ->
                        movieRecentAdapter.submitList(it1.data)
                        holder.itemView.recyclerViewMovieItem.scrollToPosition(0)
                    })
            }
            mItemClick?.let {movieRecentAdapter.onClick(it) }
        } else if (ID_UP_COMMING == holder.itemViewType) {
            settingAdapterChild(holder as ViewHolder, ID_UP_COMMING, position)
        } else if (ID_POPULAR == holder.itemViewType) {
            settingAdapterChild(holder as ViewHolder, ID_POPULAR, position)
        } else if (ID_TOP_RATED == holder.itemViewType) {
            settingAdapterChild(holder as ViewHolder, ID_TOP_RATED, position)
        }
    }

    private fun settingAdapterChild(holder: ViewHolder, action: Int, position: Int) {
        holder.bind(mListParentMovie[position])
        val linearLayoutManager = LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false)
        holder.itemView.run {
            recyclerViewMovieItem.layoutManager = linearLayoutManager
            val horizontalSpaceItemDecoration = HorizontalSpaceItemDecoration(20)
            recyclerViewMovieItem.addItemDecoration(horizontalSpaceItemDecoration)
//            val movieAdapter = MoviesRxAdapter()
            val movieAdapter = MovieAdapter()
            mItemClick?.let { movieAdapter.onClick(it) }
            mViewLifecycleOwner?.let { movieAdapter.setViewLifeCyclerOwner(it) }
            recyclerViewMovieItem.adapter = movieAdapter
            val startSnapHelper = StartSnapHelper()
            startSnapHelper.attachToRecyclerView(recyclerViewMovieItem)
            mViewLifecycleOwner?.let {
                mViewModelMovie.getFetchMovieZip()
                    .observe(it, androidx.lifecycle.Observer { listMovie ->
                        when (action) {
                            ID_POPULAR -> {
                                movieAdapter.submitList(listMovie?.get(0)?.movieList)
                            }
                            ID_UP_COMMING -> {
                                Log.i("TAG", "A" + listMovie?.get(1)?.movieList.toString())
                                movieAdapter.submitList(listMovie?.get(1)?.movieList)
                            }
                            ID_TOP_RATED -> {
                                movieAdapter.submitList(listMovie?.get(2)?.movieList)
                            }
                        }
                    })
            }
            when (action) {
                ID_POPULAR -> {
                    holder.itemView.textViewSeeMore.setOnClickListener {
                        mItemClickLoadMore?.clickItem(ID_POPULAR)
                    }
                }
                ID_UP_COMMING -> {
                    holder.itemView.textViewSeeMore.setOnClickListener {
                        mItemClickLoadMore?.clickItem(ID_UP_COMMING)
                    }
                }
                ID_TOP_RATED -> {
                    holder.itemView.textViewSeeMore.setOnClickListener {
                        mItemClickLoadMore?.clickItem(ID_TOP_RATED)
                    }
                }
            }
//            when (action) {
//                ID_POPULAR -> {
//                    mDisposable.add(mViewModelMovie.getMovie().subscribe {
//                        mLifecycle?.let { it1 -> movieAdapter.submitData(it1, it) }
//                    })
//                }
//                ID_UP_COMMING -> {
//                    mDisposable.add(mViewModelMovie.getUpcomingMovie().subscribe {
//                        mLifecycle?.let { it1 -> movieAdapter.submitData(it1, it) }
//                    })
//                }
//                ID_TOP_RATED -> {
//                    mDisposable.add(mViewModelMovie.getTopRatedMovie().subscribe {
//                        mLifecycle?.let { it1 -> movieAdapter.submitData(it1, it) }
//                    })
//                }
//                else -> {
//                    mDisposable.add(mViewModelMovie.getMovie().subscribe {
//                        mLifecycle?.let { it1 -> movieAdapter.submitData(it1, it) }
//                    })
//                }
//            }
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(item: ParentMovie) {
            itemView.textViewTitle.text = item.name
        }
    }

    companion object {
        private val COMPARATOR = object : DiffUtil.ItemCallback<ParentMovie>() {
            override fun areItemsTheSame(oldItem: ParentMovie, newItem: ParentMovie): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: ParentMovie, newItem: ParentMovie): Boolean {
                return oldItem.id == newItem.id
            }
        }
    }
}
