package com.example.appmovie.ui.movie.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.appmovie.R
import com.example.appmovie.data.model.Movie
import com.example.appmovie.data.model.favorite.MovieFavorite
import com.example.appmovie.ui.movie.listmovie.ItemClick
import com.example.appmovie.utils.Config
import kotlinx.android.synthetic.main.movie_vertical_item.view.*

class SearchMovieRxAdapter : PagingDataAdapter<Movie, SearchMovieRxAdapter.MovieVerticalViewHolder>(
    COMPARATOR
) {
    private var itemClick: ItemClick<Movie>? = null

    fun onClick(itemClick: ItemClick<Movie>) {
        this.itemClick = itemClick
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieVerticalViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.movie_vertical_item, parent, false)
        return MovieVerticalViewHolder(view)
    }

    override fun onBindViewHolder(holder: MovieVerticalViewHolder, position: Int) {
        Log.i("TAG", position.toString())
        getItem(position)?.let {
            holder.bind(it)
        }
    }

    inner class MovieVerticalViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(movie: Movie) {
            val moviePosterURL = Config.POSTER_BASE_URL + movie.posterPath
            itemView.apply {
                textViewNameMovie.text = movie.title
                Glide.with(this).load(moviePosterURL).into(imageViewPoster)
                textViewOverView.text = movie.overView
            }
            itemView.setOnClickListener {
                itemClick?.clickItem(movie)
            }
        }
    }

    companion object {
        private val COMPARATOR = object : DiffUtil.ItemCallback<Movie>() {
            override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean {
                return oldItem == newItem
            }
        }
    }
}
