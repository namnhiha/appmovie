package com.example.appmovie.ui.movie.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.appmovie.R
import com.example.appmovie.data.model.Movie
import com.example.appmovie.utils.Config
import kotlinx.android.synthetic.main.item_child_slider.view.*
import java.util.concurrent.Executors

class SliderAdapter() : ListAdapter<Movie, SliderAdapter.ViewHolder>(
    AsyncDifferConfig.Builder(COMPARATOR)
        .setBackgroundThreadExecutor(Executors.newSingleThreadExecutor())
        .build()
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SliderAdapter.ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_child_slider, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: SliderAdapter.ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(movie: Movie) {
            val moviePosterURL = Config.POSTER_BASE_URL + movie.posterPath
            Glide.with(itemView.context).load(moviePosterURL).into(itemView.imageViewPhoto)
        }
    }

    override fun submitList(list: MutableList<Movie>?) {
        super.submitList(ArrayList<Movie>(list ?: listOf()))
    }

    companion object {
        private val COMPARATOR = object : DiffUtil.ItemCallback<Movie>() {
            override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean {
                return oldItem == newItem
            }
        }
    }
}
