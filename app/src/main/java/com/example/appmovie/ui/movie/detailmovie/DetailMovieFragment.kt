package com.example.appmovie.ui.movie.detailmovie

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.appmovie.R
import com.example.appmovie.data.model.Movie
import com.example.appmovie.data.model.ParentMovie
import com.example.appmovie.data.model.favorite.MovieFavorite
import com.example.appmovie.data.model.recents.MovieRecent
import com.example.appmovie.ui.movie.adapter.HorizontalSpaceItemDecoration
import com.example.appmovie.ui.movie.detailmovie.adapter.CastAdapter
import com.example.appmovie.ui.movie.detailmovie.adapter.GeneresAdapter
import com.example.appmovie.ui.movie.detailmovie.adapter.ParentDetailMovieRxAdapter
import com.example.appmovie.utils.*
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_detail_movie.*
import javax.inject.Inject

class DetailMovieFragment : DaggerFragment(R.layout.fragment_detail_movie) {
    lateinit var mMovie: Movie
    lateinit var mCastMovieAdapter: CastAdapter
    lateinit var mParentDetailMovieRxAdapter: ParentDetailMovieRxAdapter
    lateinit var mGenresMovieAdapter: GeneresAdapter
    private var mCheckFavorite: Boolean = false

    @Inject
    lateinit var mViewModelFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mCastMovieAdapter = CastAdapter()
        mGenresMovieAdapter = GeneresAdapter()
        mDetailMovieViewModel =
            ViewModelProvider(this, mViewModelFactory).get(DetailMovieViewModel::class.java)
        arguments?.let {
            mMovie = it.getParcelable(ID_DETAIL_MUSIC)!!
        }
        mDetailMovieViewModel.deleteAndCreateMovieRecent(
            MovieRecent(
                0,
                mMovie.id,
                mMovie.title,
                mMovie.posterPath,
                mMovie.releaseDate,
                mMovie.voteAverage,
                mMovie.voteCount,
                mMovie.overView,
                false
            )
        )
        mDetailMovieViewModel.fetchDataCastAndGenres(mMovie.id)
    }

    @SuppressLint("CheckResult")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerViewActorMovie.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        val horizontalSpaceItemDecorationCast = HorizontalSpaceItemDecoration(50)
        recyclerViewActorMovie.addItemDecoration(horizontalSpaceItemDecorationCast)
        recyclerViewActorMovie.adapter = mCastMovieAdapter
        recyclerViewGenere.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        val horizontalSpaceItemDecorationGenres = HorizontalSpaceItemDecoration(25)
        recyclerViewGenere.addItemDecoration(horizontalSpaceItemDecorationGenres)
        recyclerViewGenere.adapter = mGenresMovieAdapter
        val moviePosterURL = Config.POSTER_BASE_URL + mMovie.posterPath
        Glide.with(this).load(moviePosterURL).into(imageViewDetail)
        Glide.with(this).load(moviePosterURL).into(imageViewDetailLarge)
        textViewReleateData.text = mMovie.releaseDate
        textViewTitle.text = mMovie.title
        textViewOverView.text = mMovie.overView
        mDetailMovieViewModel.getDataCast().observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    mCastMovieAdapter.submitList(it.data)
                }
                Status.RUNNING -> {
                }
                Status.FAILED -> {
                }
            }
        })
        mDetailMovieViewModel.getDataDetail().observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    it.data?.let { it1 -> mDetailMovieViewModel.createCategory(it1) }
                }
                Status.RUNNING -> {}
                Status.FAILED -> {}
            }
        })
        mDetailMovieViewModel.getDataGenres().observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    mGenresMovieAdapter.submitList(it.data)
                }
                Status.RUNNING -> {
                }
                Status.FAILED -> {
                }
            }
        })

        mParentDetailMovieRxAdapter = ParentDetailMovieRxAdapter()
//        mParentDetailMovieRxAdapter.setItemClick(this)
        recyclerViewDetailMovie.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        recyclerViewDetailMovie.adapter = mParentDetailMovieRxAdapter
        val dividerItemDecoration = DividerItemDecoration(
            context, DividerItemDecoration.HORIZONTAL
        )
        recyclerViewDetailMovie.addItemDecoration(dividerItemDecoration)
        context?.let {
            mParentDetailMovieRxAdapter.setData(
                arrayListOf(
                    ParentMovie(ID_SIMILAR_MOVIE, SIMILAR_MOVIE),
                    ParentMovie(ID_RECOMENDATION_MOVIE, RECOMENDATION_MOVIE),
                ),
                it, lifecycle, mMovie.id, viewLifecycleOwner
            )
        }

        mDetailMovieViewModel.getMovieIdFavorite(mMovie.id).observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    if (it.data?.checkFavorite == true) {
                        imageViewBookMark.setBackgroundResource(R.drawable.ic_book_mark_yellow)
                        imageSaveFavorite.setBackgroundResource(R.drawable.ic_favorite_yellow)
                    } else {
                        imageViewBookMark.setBackgroundResource(R.drawable.bookmark)
                        imageSaveFavorite.setBackgroundResource(R.drawable.ic_save_favorite)
                    }
                }
                Status.RUNNING -> {
                }
                Status.FAILED -> {
                }
            }
        })
    }

    @SuppressLint("CheckResult")
    override fun onResume() {
        super.onResume()
        ratingBarDetailMovie.rating = mMovie.voteAverage
        imageViewBookMark.setOnClickListener {
            when (mCheckFavorite) {
                false -> {
                    mCheckFavorite = true
                    mMovie.run {
                        val movieFavorite = MovieFavorite(
                            id,
                            title,
                            posterPath,
                            releaseDate,
                            voteAverage,
                            voteCount,
                            overView,
                            true
                        )
                        imageViewBookMark.setBackgroundResource(R.drawable.ic_book_mark_yellow)
                        imageSaveFavorite.setBackgroundResource(R.drawable.ic_favorite_yellow)
                        mDetailMovieViewModel.createMovieFavorite(movieFavorite)
                    }
                }
                true -> {
                    mCheckFavorite = false
                    mMovie.run {
                        val movieFavorite = MovieFavorite(
                            id,
                            title,
                            posterPath,
                            releaseDate,
                            voteAverage,
                            voteCount,
                            overView,
                            true
                        )
                        imageViewBookMark.setBackgroundResource(R.drawable.bookmark)
                        imageSaveFavorite.setBackgroundResource(R.drawable.ic_favorite)
                        mDetailMovieViewModel.deleteMovie(movieFavorite)
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    companion object {
        lateinit var mDetailMovieViewModel: DetailMovieViewModel
        const val ID_DETAIL_MUSIC = "ID_DETAIL_MUSIC"
        fun newInstance(item: Movie) = DetailMovieFragment().apply {
            arguments = Bundle().apply {
                putParcelable(ID_DETAIL_MUSIC, item)
            }
        }
    }
}
