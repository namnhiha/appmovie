package com.example.appmovie.ui.movie.detailmovie

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.rxjava2.cachedIn
import com.example.appmovie.data.model.Movie
import com.example.appmovie.data.model.detail.CastMovie
import com.example.appmovie.data.model.detail.DetailMovie
import com.example.appmovie.data.model.detail.Genres
import com.example.appmovie.data.model.favorite.CategoryGenres
import com.example.appmovie.data.model.favorite.MovieFavorite
import com.example.appmovie.data.model.recents.MovieRecent
import com.example.appmovie.data.repository.DetailRepository
import com.example.appmovie.utils.Resource
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class DetailMovieViewModel @Inject constructor(private var repository: DetailRepository) :
    ViewModel() {
    private val mMovieCredit = MutableLiveData<Resource<MutableList<CastMovie>>>()
    private val mMovieGenres = MutableLiveData<Resource<MutableList<Genres>>>()
    private val mMovieSimilar = MutableLiveData<Resource<PagingData<Movie>>>()
    private val mMovieRecomendation = MutableLiveData<Resource<PagingData<Movie>>>()
    private val mMovieIdFavorite = MutableLiveData<Resource<MovieFavorite>>()
    private val mMovieFavorite = MutableLiveData<Resource<Long>>()
    private val mMovieDetail = MutableLiveData<Resource<DetailMovie>>()

    fun fetchDataCastAndGenres(movieId: Int) {
        mDisposable.addAll(
            repository.getCastMovie(movieId).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(
                    {
                        mMovieCredit.postValue(Resource.success(it.cast))
                    },
                    {
                        mMovieGenres.postValue(Resource.error("Something Error", null))
                    }
                ),
            repository.getGenresMovie(movieId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(
                    {
                        mMovieDetail.postValue(Resource.success(it))
                        mMovieGenres.postValue(Resource.success(it.genres))
                    }, {
                        mMovieDetail.postValue(Resource.error("Something Error", null))
                        mMovieGenres.postValue(Resource.error("Something Error", null))
                    }
                ),
            repository.getSimilarMovie(movieId)
                .map { it.mapSync { item -> item.copy(voteAverage = (Math.round(item.voteAverage * 5 / 10)).toFloat()) } }
                .cachedIn(viewModelScope).subscribe(
                    {
                        mMovieSimilar.postValue(Resource.success(it))
                    },
                    {
                        mMovieSimilar.postValue(Resource.error("Something Error", null))
                    }
                ),
            repository.getRecomendationMovie(movieId)
                .map { it.mapSync { item -> item.copy(voteAverage = (Math.round(item.voteAverage * 5 / 10)).toFloat()) } }
                .cachedIn(viewModelScope).subscribe(
                    {
                        mMovieRecomendation.postValue(Resource.success(it))
                    },
                    {
                        mMovieRecomendation.postValue(Resource.error("Something Error", null))
                    }
                ),
            repository.getMovieIdFavorite(movieId).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(
                    {
                        mMovieIdFavorite.postValue(Resource.success(it))
                    },
                    {
                        mMovieIdFavorite.postValue(Resource.error("Something Error", null))
                    }
                ),
        )
    }

    @SuppressLint("CheckResult")
    fun createMovieFavorite(movieFavorite: MovieFavorite) {
        Single.fromCallable {
            repository.createMovieFavorite(movieFavorite)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()).subscribe(
                {
                    mMovieFavorite.postValue(Resource.success(it))
                },
                {
                    mMovieFavorite.postValue(Resource.error("Something Error", null))
                }
            )
    }

    @SuppressLint("CheckResult")
    fun createCategoryGenres(categoryGenres: MutableList<CategoryGenres>,id: Int) {
        Single.fromCallable {
            repository.createCategoryGenres(categoryGenres,id)
        }.subscribeOn(Schedulers.io())
            .subscribe()
    }

    @SuppressLint("CheckResult")
    fun createCategory(movie: DetailMovie) {
        val categoryGenres = mutableListOf<CategoryGenres>()
        Single.fromCallable {
            for (i in movie.genres) {
                categoryGenres.add(CategoryGenres(i.id, i.name))
            }
        }.subscribeOn(Schedulers.newThread())
            .subscribe()
        createCategoryGenres(categoryGenres,movie.id)
    }

    @SuppressLint("CheckResult")
    fun deleteMovie(movieFavorite: MovieFavorite) {
        Single.fromCallable {
            repository.deleteMovieFavorite(movieFavorite)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    @SuppressLint("CheckResult")
    fun deleteAndCreateMovieRecent(movieRecent: MovieRecent) {
        Single.fromCallable {
            repository.deleteAndCreateMovieRecent(movieRecent)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()).subscribe()
    }

    fun getDataCast(): LiveData<Resource<MutableList<CastMovie>>> {
        return mMovieCredit
    }

    fun getDataGenres(): LiveData<Resource<MutableList<Genres>>> {
        return mMovieGenres
    }

    fun getDataDetail(): LiveData<Resource<DetailMovie>> {
        return mMovieDetail
    }

    fun getDataSimilarMovie(): LiveData<Resource<PagingData<Movie>>> {
        return mMovieSimilar
    }

    fun getDataRecomendationMovie(): LiveData<Resource<PagingData<Movie>>> {
        return mMovieRecomendation
    }

    fun getMovieIdFavorite(id: Int): LiveData<Resource<MovieFavorite>> {
        return mMovieIdFavorite
    }

    override fun onCleared() {
        super.onCleared()
        mDisposable.clear()
    }

    companion object {
        val mDisposable = CompositeDisposable()
    }
}
