package com.example.appmovie.ui.movie.detailmovie.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.appmovie.R
import com.example.appmovie.data.model.detail.CastMovie
import com.example.appmovie.utils.Config
import kotlinx.android.synthetic.main.cast_movie_horizontal.view.*
import java.util.concurrent.Executors

class CastAdapter : ListAdapter<CastMovie, CastAdapter.CastHolder>(
    AsyncDifferConfig.Builder(COMPARATOR)
        .setBackgroundThreadExecutor(Executors.newSingleThreadExecutor())
        .build()
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CastHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.cast_movie_horizontal, parent, false)
        return CastHolder(view)
    }

    override fun onBindViewHolder(holder: CastHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class CastHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(castMovie: CastMovie) {
            val moviePosterURL = Config.POSTER_BASE_URL + castMovie.profilePath
            itemView.let {
                Glide.with(it).load(moviePosterURL).into(it.imageViewCast)
                it.textViewNameCast.text = castMovie.name
            }
        }
    }

    override fun submitList(list: MutableList<CastMovie>?) {
        super.submitList(ArrayList<CastMovie>(list ?: listOf()))
    }

    companion object {
        private val COMPARATOR = object : DiffUtil.ItemCallback<CastMovie>() {
            override fun areItemsTheSame(oldItem: CastMovie, newItem: CastMovie): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: CastMovie, newItem: CastMovie): Boolean {
                return oldItem.id == newItem.id
            }
        }
    }
}
