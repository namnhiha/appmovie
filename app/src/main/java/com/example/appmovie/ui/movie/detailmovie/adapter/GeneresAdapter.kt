package com.example.appmovie.ui.movie.detailmovie.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.appmovie.R
import com.example.appmovie.data.model.detail.Genres
import kotlinx.android.synthetic.main.genres_item.view.*
import java.util.concurrent.Executors

class GeneresAdapter : ListAdapter<Genres, GeneresAdapter.GenresViewHolder>(
    AsyncDifferConfig.Builder(COMPARATOR)
        .setBackgroundThreadExecutor(Executors.newSingleThreadExecutor())
        .build()
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenresViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.genres_item, parent, false)
        return GenresViewHolder(view)
    }

    override fun onBindViewHolder(holder: GenresViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class GenresViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        fun bind(genres: Genres) {
            itemView.let {
                it.textViewGenres.text = genres.name
            }
        }
    }

    override fun submitList(list: MutableList<Genres>?) {
        super.submitList(ArrayList<Genres>(list ?: listOf()))
    }

    companion object {
        private val COMPARATOR = object : DiffUtil.ItemCallback<Genres>() {
            override fun areItemsTheSame(oldItem: Genres, newItem: Genres): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: Genres, newItem: Genres): Boolean {
                return oldItem.id == newItem.id
            }
        }
    }
}
