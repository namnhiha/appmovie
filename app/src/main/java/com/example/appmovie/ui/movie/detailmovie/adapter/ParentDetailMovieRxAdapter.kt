package com.example.appmovie.ui.movie.detailmovie.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.appmovie.R
import com.example.appmovie.data.model.Movie
import com.example.appmovie.data.model.ParentMovie
import com.example.appmovie.ui.movie.adapter.DetailMoviesRxAdapter
import com.example.appmovie.ui.movie.adapter.HorizontalSpaceItemDecoration
import com.example.appmovie.ui.movie.detailmovie.DetailMovieFragment
import com.example.appmovie.ui.movie.listmovie.ItemClick
import com.example.appmovie.utils.ID_RECOMENDATION_MOVIE
import com.example.appmovie.utils.ID_SIMILAR_MOVIE
import com.example.appmovie.utils.Status
import kotlinx.android.synthetic.main.recyclerview_item.view.*
import java.util.*

class ParentDetailMovieRxAdapter : ListAdapter<ParentMovie, RecyclerView.ViewHolder>(
    COMPARATOR
) {
    private var mListParentMovie = arrayListOf<ParentMovie>()
    private var mContext: Context? = null
    private var mLifecycle: Lifecycle? = null
    private var mItemClick: ItemClick<Movie>? = null
    private var mMovieId: Int? = null
    private var mViewLifecycleOwner: LifecycleOwner? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.recyclerview_item, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setData(
        list: ArrayList<ParentMovie>,
        context: Context,
        lifecycle: Lifecycle,
        movieId: Int,
        viewLifecycleOwner: LifecycleOwner
    ) {
        mListParentMovie = list
        mContext = context
        mLifecycle = lifecycle
        mMovieId = movieId
        mViewLifecycleOwner = viewLifecycleOwner
        notifyDataSetChanged()
    }

    fun setItemClick(item: ItemClick<Movie>) {
        this.mItemClick = item
    }

    override fun getItemCount() = mListParentMovie.size

    override fun getItemViewType(position: Int): Int {
        return mListParentMovie[position].id
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (ID_SIMILAR_MOVIE == holder.itemViewType) {
            settingAdapterChild(
                holder as ParentDetailMovieRxAdapter.ViewHolder,
                ID_SIMILAR_MOVIE,
                position
            )
        } else if (ID_RECOMENDATION_MOVIE == holder.itemViewType) {
            settingAdapterChild(
                holder as ParentDetailMovieRxAdapter.ViewHolder,
                ID_RECOMENDATION_MOVIE, position
            )
        }
    }

    private fun settingAdapterChild(
        holder: ParentDetailMovieRxAdapter.ViewHolder,
        action: Int,
        position: Int
    ) {
        holder.bind(mListParentMovie[position])
        val linearLayoutManager = LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false)
        holder.itemView.run {
            recyclerViewMovieItem.layoutManager = linearLayoutManager
            val horizontalSpaceItemDecoration = HorizontalSpaceItemDecoration(20)
            recyclerViewMovieItem.addItemDecoration(horizontalSpaceItemDecoration)
            val movieAdapter = DetailMoviesRxAdapter()
//            mItemClick?.let { movieAdapter.onClick(it) }
            recyclerViewMovieItem.adapter = movieAdapter
            when (action) {
                ID_SIMILAR_MOVIE -> {
                    mMovieId?.let {
                        mViewLifecycleOwner?.let { it1 ->
                            DetailMovieFragment.mDetailMovieViewModel.getDataSimilarMovie().observe(
                                it1, androidx.lifecycle.Observer {
                                    when (it.status) {
                                        Status.SUCCESS -> {
                                            mLifecycle?.let { it1 ->
                                                it.data?.let { it2 ->
                                                    movieAdapter.submitData(
                                                        it1,
                                                        it2
                                                    )
                                                }
                                            }
                                        }
                                        Status.RUNNING -> {
                                        }
                                        Status.FAILED -> {
                                        }
                                    }
                                }
                            )
                        }
                    }
                }
                ID_RECOMENDATION_MOVIE -> {
                    mMovieId?.let {
                        mViewLifecycleOwner?.let { it1 ->
                            DetailMovieFragment.mDetailMovieViewModel.getDataRecomendationMovie()
                                .observe(
                                    it1, androidx.lifecycle.Observer {
                                        when (it.status) {
                                            Status.SUCCESS -> {
                                                mLifecycle?.let { it1 ->
                                                    it.data?.let { it2 ->
                                                        movieAdapter.submitData(
                                                            it1,
                                                            it2
                                                        )
                                                    }
                                                }
                                            }
                                            Status.RUNNING -> {
                                            }
                                            Status.FAILED -> {
                                            }
                                        }
                                    }
                                )
                        }
                    }
                }
                else -> {}
            }
        }
    }

    inner class ViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        fun bind(item: ParentMovie) {
            itemView.textViewTitle.text = item.name
        }
    }

    companion object {
        private val COMPARATOR = object : DiffUtil.ItemCallback<ParentMovie>() {
            override fun areItemsTheSame(oldItem: ParentMovie, newItem: ParentMovie): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: ParentMovie, newItem: ParentMovie): Boolean {
                return oldItem.id == newItem.id
            }
        }
    }
}
