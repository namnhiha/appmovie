package com.example.appmovie.ui.movie.favoritemovie

interface BottomNavigationView<T> {
    fun click(check: T)
}
