package com.example.appmovie.ui.movie.favoritemovie

object ExpandableListDataPump {
    val data: HashMap<String, MutableList<String>>
        get() {
            val expandableListDetail = HashMap<String, MutableList<String>>()
            val sortOrder: MutableList<String> = ArrayList()
            sortOrder.add("List Order")
            sortOrder.add("Release Date")
            sortOrder.add("Popularity")
            val genres: MutableList<String> = ArrayList()
            genres.add("Action")
            genres.add("Adventure")
            expandableListDetail["Sort By"] = sortOrder
            expandableListDetail["Genres"] = genres
            return expandableListDetail
        }
}
