package com.example.appmovie.ui.movie.favoritemovie

import android.graphics.*
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appmovie.R
import com.example.appmovie.data.model.Movie
import com.example.appmovie.data.model.favorite.MovieFavorite
import com.example.appmovie.ui.movie.detailmovie.DetailMovieFragment
import com.example.appmovie.ui.movie.favoritemovie.adapter.CustomExpandableListAdapter
import com.example.appmovie.ui.movie.favoritemovie.adapter.FavoriteAdapter
import com.example.appmovie.ui.movie.favoritemovie.adapter.VerticalSpaceItemDecoration
import com.example.appmovie.ui.movie.listmovie.ItemClick
import com.example.appmovie.utils.Status
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_favorite_movie.*
import javax.inject.Inject

class FavoriteMovieFragment : DaggerFragment(R.layout.fragment_favorite_movie), ItemClick<Movie> {
    lateinit var mExpandableListTitle: MutableList<String>
    lateinit var mExpandableListDetail: HashMap<String, MutableList<String>>
    lateinit var mExpandableAdapter: CustomExpandableListAdapter
    lateinit var mFavoriteAdapter: FavoriteAdapter
    private var mHideShowBottomNavigationView = true
    private var mPaint = Paint()
    private var mListFavorite: MutableList<MovieFavorite>? = null

    @Inject
    lateinit var mViewModelFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mExpandableListDetail = ExpandableListDataPump.data
        mExpandableListTitle = ArrayList(mExpandableListDetail.keys)
        mFavoriteAdapter = FavoriteAdapter()
        mFavoriteMovieViewModel =
            ViewModelProvider(this, mViewModelFactory).get(FavoriteMovieViewModel::class.java)
//        (activity as AppCompatActivity?)!!.setSupportActionBar(toolBarFavorite)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mFavoriteAdapter.setItemClick(this)
        mFavoriteMovieViewModel.getCategoryGenres().observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    it.data?.let {
                        mExpandableListDetail["Genres"] = it
                    }
                }
                Status.RUNNING -> {}
                Status.FAILED -> {}
            }
        })
        activity?.let {
            mExpandableAdapter = CustomExpandableListAdapter(
                it.applicationContext,
                mExpandableListTitle as ArrayList<String>, mExpandableListDetail
            )
        }
        expendableListView.setAdapter(mExpandableAdapter)
        recyclerViewMovie.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        recyclerViewMovie.adapter = mFavoriteAdapter
        recyclerViewMovie.addItemDecoration(VerticalSpaceItemDecoration(24))
        mFavoriteMovieViewModel.getMovieFavorite().observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    mFavoriteAdapter.submitList(it.data)
                    toolBarFavorite.title = "${it.data?.size ?: 0} Result"
                }
                Status.RUNNING -> {
                }
                Status.FAILED -> {
                }
            }
        })
        mFavoriteMovieViewModel.getMovieFavoriteGenres().observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    mFavoriteAdapter.submitList(it.data)
                    toolBarFavorite.title = "${it.data?.size ?: 0} Result"
                }
                Status.RUNNING -> {}
                Status.FAILED -> {}
            }
        })
    }

    override fun onResume() {
        super.onResume()
        val simpleItemTouchCallback = object :
            ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition
                if (direction == ItemTouchHelper.LEFT) {
                    mFavoriteAdapter.swipe(position)
                    Toast.makeText(requireContext(), "Deleted", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onChildDraw(
                c: Canvas,
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                dX: Float,
                dY: Float,
                actionState: Int,
                isCurrentlyActive: Boolean
            ) {
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                    val itemView = viewHolder.itemView
                    val height = itemView.bottom.toFloat() - itemView.top.toFloat()
                    val width = height / 3

                    if (dX < 0) {
                        mPaint.color = Color.WHITE
                        val background = RectF(
                            itemView.right.toFloat() + dX,
                            itemView.top.toFloat(),
                            itemView.right.toFloat(),
                            itemView.bottom.toFloat()
                        )
                        c.drawRect(background, mPaint)
                        val icon = BitmapFactory.decodeResource(
                            activity?.resources,
                            R.drawable.delete_forever
                        )
                        val margin = (dX / 5 - width) / 2
                        val iconDest = RectF(
                            itemView.right.toFloat() + margin,
                            itemView.top.toFloat() + width,
                            itemView.right.toFloat() + (margin + width),
                            itemView.bottom.toFloat() - width
                        )
                        c.drawBitmap(icon, null, iconDest, mPaint)
                    }
                } else {
                    c.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR)
                }
                super.onChildDraw(
                    c,
                    recyclerView,
                    viewHolder,
                    dX / 2,
                    dY,
                    actionState,
                    isCurrentlyActive
                )
            }
        }
        val itemTouchHelper = ItemTouchHelper(simpleItemTouchCallback)
        itemTouchHelper.attachToRecyclerView(recyclerViewMovie)
        toolBarFavorite.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.action_remove -> {
                    when (mHideShowBottomNavigationView) {
                        true -> {
                            mFavoriteAdapter.updateCheckList(true)
                            buttonRemove.visibility = View.VISIBLE
                            parentFragmentManager.setFragmentResult(
                                "requestKey",
                                bundleOf("bundleKey" to mHideShowBottomNavigationView)
                            )
                            mHideShowBottomNavigationView = false
                        }
                        false -> {
                            mFavoriteAdapter.updateCheckList(false)
                            buttonRemove.visibility = View.INVISIBLE
                            parentFragmentManager.setFragmentResult(
                                "requestKey",
                                bundleOf("bundleKey" to mHideShowBottomNavigationView)
                            )
                            mHideShowBottomNavigationView = true
                        }
                    }
                    true
                }
                R.id.action_sortby -> {
                    expendableListView.visibility = View.VISIBLE
                    true
                }
                else -> true
            }
        }
        buttonRemove.setOnClickListener {
            mFavoriteMovieViewModel.deleteMovieFavorite()
            mFavoriteMovieViewModel.mListRemove.addAll(mutableListOf())
        }
        expendableListView.setOnGroupCollapseListener { groupPosition ->
            Toast.makeText(
                activity,
                mExpandableListTitle.get(groupPosition) + " List Collapsed.",
                Toast.LENGTH_SHORT
            ).show()
        }
        mFavoriteMovieViewModel.getCategoryGenres().observe(viewLifecycleOwner, Observer {
            expendableListView.setOnChildClickListener { _, v, groupPosition, childPosition, _ ->
                when (groupPosition) {
                    0 -> {
                        when (childPosition) {
                            0 -> {
                                mFavoriteMovieViewModel.getMovieFavorite()
                                    .observe(viewLifecycleOwner,
                                        Observer {
                                            mFavoriteAdapter.submitList(it.data)
                                            toolBarFavorite.title = "${it.data?.size ?: 0} Result"
                                        })
                            }
                            1 -> {
                                mFavoriteMovieViewModel.getMovieFavorite()
                                    .observe(viewLifecycleOwner,
                                        Observer {
                                            mListFavorite = it.data
                                            mListFavorite?.sortByDescending {
                                                it.releaseDate
                                            }
                                            mFavoriteAdapter.submitList(mListFavorite)
                                            toolBarFavorite.title = "${it.data?.size ?: 0} Result"
                                        })
                            }
                            2 -> {
                                mFavoriteMovieViewModel.getMovieFavorite()
                                    .observe(viewLifecycleOwner,
                                        Observer {
                                            mListFavorite = it.data
                                            mListFavorite?.sortBy {
                                                it.voteAverage
                                            }
                                            mFavoriteAdapter.submitList(mListFavorite)
                                            toolBarFavorite.title = "${it.data?.size ?: 0} Result"
                                        })
                            }
                        }
                    }
                    1 -> {
                        for (i in 0..(it.data?.size ?: 0)) {
                            if (childPosition == i) {
                                val list = mutableListOf<String>()
                                list.add(it.data!![i])
                                mFavoriteMovieViewModel.getMovieFavoriteGeneres(list)
                            }
                        }
                    }
                }
                expendableListView.visibility = View.INVISIBLE
                return@setOnChildClickListener false
            }
        })
    }

    override fun clickItem(item: Movie) {
        parentFragmentManager.beginTransaction()
            .replace(R.id.fragmentContainer, DetailMovieFragment.newInstance(item))
            .addToBackStack(null)
            .commit()
    }

    companion object {
        lateinit var mFavoriteMovieViewModel: FavoriteMovieViewModel
        fun newInstance() = FavoriteMovieFragment()
    }
}
