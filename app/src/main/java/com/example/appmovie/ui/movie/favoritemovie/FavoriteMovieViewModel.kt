package com.example.appmovie.ui.movie.favoritemovie

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.appmovie.data.model.favorite.CategoryGenres
import com.example.appmovie.data.model.favorite.MovieFavorite
import com.example.appmovie.data.repository.FavoriteRepository
import com.example.appmovie.utils.Resource
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class FavoriteMovieViewModel @Inject constructor(private var repository: FavoriteRepository) :
    ViewModel() {
    private val mMovieFavorite = MutableLiveData<Resource<MutableList<MovieFavorite>>>()
    private val mList = MutableLiveData<MutableList<MovieFavorite>>()
    private val mMovieFavoriteSearchGenere = MutableLiveData<Resource<MutableList<MovieFavorite>>>()
    private var mListGenresCategory = MutableLiveData<Resource<MutableList<String>>>()
    private var mListFavorite: MutableList<MovieFavorite>? = null
    val mListRemove = mutableListOf<MovieFavorite>()

    init {
        fetchMovieFavorite()
    }

    fun fetchMovieFavorite() {
        mCompositeDisposable.addAll(
            repository.getAllMovieFavorite().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        mMovieFavorite.postValue(Resource.success(it))
                    },
                    {
                        mMovieFavorite.postValue(Resource.error("Something Error", null))
                    }
                ),
            repository.getAllCategoryGenres().subscribeOn(Schedulers.io())
                .map {
                    val list= mutableListOf<String>()
                    for (i in it)
                    {
                        list.add(i.nameCategory)
                    }
                    list
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        mListGenresCategory.postValue(Resource.success(it))
                    },
                    {
                        mListGenresCategory.postValue(Resource.error("Something Error", null))
                    }
                )
        )
    }

    fun getMovieFavoriteGeneres(list: MutableList<String>) {
        mCompositeDisposable.add(
            repository.getMovieFavoriteGenres(list).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        Log.i("TAG ACTION NHE", it.toString())
                        mMovieFavoriteSearchGenere.postValue(Resource.success(it))
                    },
                    {
                        mMovieFavoriteSearchGenere.postValue(
                            Resource.error(
                                "Something Error",
                                null
                            )
                        )
                    }
                )
        )
    }

    fun deleteMovieFavorite() {
        for (i in mListRemove) {
            Single.fromCallable {
                repository.deleteFavorite(i)
            }.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe()
        }
    }

    fun deleteSingleFavorite(movieFavorite: MovieFavorite) {
        Single.fromCallable {
            repository.deleteFavorite(movieFavorite)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun getMovieFavorite(): LiveData<Resource<MutableList<MovieFavorite>>> {
        return mMovieFavorite
    }

    fun getMovieFavoriteGenres(): LiveData<Resource<MutableList<MovieFavorite>>> {
        return mMovieFavoriteSearchGenere
    }

    fun getCategoryGenres(): LiveData<Resource<MutableList<String>>> {
        return mListGenresCategory
    }

    fun setMovie(mItem: MovieFavorite) {
        mListRemove.add(mItem)
    }

    fun removeMovie(mItem: MovieFavorite) {
        mListRemove.remove(mItem)
    }

    override fun onCleared() {
        super.onCleared()
        mCompositeDisposable.clear()
    }

    companion object {
        private var mCompositeDisposable: CompositeDisposable = CompositeDisposable()
    }
}
