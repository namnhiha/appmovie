package com.example.appmovie.ui.movie.favoritemovie.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import com.example.appmovie.R
import kotlinx.android.synthetic.main.list_favorite_group.view.*
import kotlinx.android.synthetic.main.list_favorite_item.view.*

class CustomExpandableListAdapter(
    private var mContext: Context,
    private var mExpendableListTitle: MutableList<String>,
    private var mExpendableDetail: HashMap<String, MutableList<String>>
) : BaseExpandableListAdapter() {
    override fun getGroupCount(): Int {
        return mExpendableListTitle.size
    }

    override fun getChildrenCount(groupPosition: Int): Int {
        return mExpendableDetail.get(mExpendableListTitle[groupPosition])!!.size
    }

    override fun getGroup(groupPosition: Int): Any {
        return mExpendableListTitle.get(groupPosition)
    }

    override fun getChild(groupPosition: Int, childPosition: Int): Any {
        return mExpendableDetail[mExpendableListTitle[groupPosition]]!![childPosition]
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    @SuppressLint("InflateParams")
    override fun getGroupView(
        groupPosition: Int,
        isExpanded: Boolean,
        convertView: View?,
        parent: ViewGroup?
    ): View {
        var convertViewGroup = convertView
        val listTitle = getGroup(groupPosition)
        if (convertView == null) {
            val layoutInflater =
                mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertViewGroup = layoutInflater.inflate(R.layout.list_favorite_group, null)
            convertViewGroup.listTitle.text = listTitle.toString()
        } else {
            convertViewGroup?.listTitle?.text = listTitle.toString()
        }
        return convertViewGroup!!
    }

    @SuppressLint("InflateParams")
    override fun getChildView(
        groupPosition: Int,
        childPosition: Int,
        isLastChild: Boolean,
        convertView: View?,
        parent: ViewGroup?
    ): View {
        var convertView: View? = convertView
        val expededListText = getChild(groupPosition, childPosition)
        if (convertView == null) {
            val layoutInflater =
                mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = layoutInflater.inflate(R.layout.list_favorite_item, null)
            convertView.expandedListItem.text = expededListText.toString()
        } else {
            convertView.expandedListItem.text = expededListText.toString()
        }
        return convertView!!
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return true
    }
}
