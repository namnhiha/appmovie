package com.example.appmovie.ui.movie.favoritemovie.adapter

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.appmovie.R
import com.example.appmovie.data.model.Movie
import com.example.appmovie.data.model.favorite.MovieFavorite
import com.example.appmovie.ui.movie.favoritemovie.FavoriteMovieFragment
import com.example.appmovie.ui.movie.listmovie.ItemClick
import com.example.appmovie.utils.Config
import kotlinx.android.synthetic.main.movie_vertical_item.view.*
import java.util.concurrent.Executors


class FavoriteAdapter : ListAdapter<MovieFavorite, FavoriteAdapter.FavoriteMovieHolder>(
    AsyncDifferConfig.Builder(COMPARATOR)
        .setBackgroundThreadExecutor(Executors.newSingleThreadExecutor())
        .build()
) {
    private var mShowCheckBox = false
    private var mItemClick: ItemClick<Movie>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteMovieHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.movie_vertical_item, parent, false)
        return FavoriteMovieHolder(view)
    }

    fun setItemClick(item: ItemClick<Movie>) {
        this.mItemClick = item
    }

    @SuppressLint("NotifyDataSetChanged")
    fun updateCheckList(mShowCheckBox: Boolean) {
        this.mShowCheckBox = mShowCheckBox
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: FavoriteMovieHolder, position: Int) {
        holder.bind(getItem(position))
        Log.i("TAG", getItem(position).releaseDate)
        holder.itemView.checkBox.visibility =
            if (mShowCheckBox) View.VISIBLE else View.INVISIBLE
    }

    inner class FavoriteMovieHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(movieFavorite: MovieFavorite) {
            val moviePosterURL = Config.POSTER_BASE_URL + movieFavorite.posterPath
            itemView.let {
                Glide.with(it).load(moviePosterURL).into(it.imageViewPoster)
                it.textViewNameMovie.text = movieFavorite.title
                it.textViewOverView.text = movieFavorite.releaseDate
                Log.i("TAG", movieFavorite.voteAverage.toString())
                it.ratingBarMovieFavorite.rating = movieFavorite.voteAverage
            }
            if (itemView.checkBox.isChecked) {
                itemView.checkBox.isChecked = false
            }
            itemView.checkBox.setOnClickListener {
                if (itemView.checkBox.isChecked) {
                    FavoriteMovieFragment.mFavoriteMovieViewModel.setMovie(movieFavorite)
                } else {
                    FavoriteMovieFragment.mFavoriteMovieViewModel.removeMovie(movieFavorite)
                }
            }
            itemView.setOnClickListener {
                mItemClick?.clickItem(
                    Movie(
                        movieFavorite.id,
                        movieFavorite.posterPath,
                        movieFavorite.releaseDate,
                        movieFavorite.title,
                        movieFavorite.voteAverage,
                        movieFavorite.voteCount,
                        movieFavorite.overView
                    )
                )
            }
        }
    }

    override fun submitList(list: MutableList<MovieFavorite>?) {
        super.submitList(ArrayList<MovieFavorite>(list ?: listOf()))
    }

    fun swipe(position: Int) {
        FavoriteMovieFragment.mFavoriteMovieViewModel.deleteSingleFavorite(getItem(position))
    }

    companion object {
        private val COMPARATOR = object : DiffUtil.ItemCallback<MovieFavorite>() {
            override fun areItemsTheSame(oldItem: MovieFavorite, newItem: MovieFavorite): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(
                oldItem: MovieFavorite,
                newItem: MovieFavorite
            ): Boolean {
                return oldItem.id == newItem.id
            }
        }
    }
}
