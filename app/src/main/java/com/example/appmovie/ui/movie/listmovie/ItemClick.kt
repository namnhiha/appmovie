package com.example.appmovie.ui.movie.listmovie

interface ItemClick<T> {
    fun clickItem(item:T)
}
