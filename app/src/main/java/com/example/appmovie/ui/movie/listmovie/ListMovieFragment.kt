package com.example.appmovie.ui.movie.listmovie

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import com.example.appmovie.R
import com.example.appmovie.data.model.Movie
import com.example.appmovie.data.model.ParentMovie
import com.example.appmovie.ui.movie.adapter.ParentMovieRxAdapter
import com.example.appmovie.ui.movie.adapter.SliderAdapter
import com.example.appmovie.ui.movie.detailmovie.DetailMovieFragment
import com.example.appmovie.ui.movie.favoritemovie.adapter.VerticalSpaceItemDecoration
import com.example.appmovie.ui.movie.morelistmovie.MoreMovieFragment
import com.example.appmovie.utils.*
import dagger.android.support.DaggerFragment
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_list_movie.*
import java.util.*
import javax.inject.Inject

class ListMovieFragment : DaggerFragment(R.layout.fragment_list_movie), ItemClick<Movie>,
    ItemClickLoadMore {
    @Inject
    lateinit var mViewModelFactory: ViewModelProvider.Factory
    private lateinit var mAdapter: ParentMovieRxAdapter
    lateinit var mTimer: Timer
    lateinit var mTimerTask: TimerTask
    lateinit var mLayoutManager: LinearLayoutManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModelMovie =
            ViewModelProvider(this, mViewModelFactory).get(ListMovieViewModel::class.java)
    }

    @SuppressLint("CheckResult")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mAdapter = ParentMovieRxAdapter()
        mAdapter.setItemClick(this)
        mAdapter.setItemClickLoadMore(this)
        recyclerViewMovie.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        recyclerViewMovie.adapter = mAdapter
        val verticalSpaceItemDecoration = VerticalSpaceItemDecoration(20)
        recyclerViewMovie.addItemDecoration(verticalSpaceItemDecoration)
        context?.let {
            mAdapter.setData(
                arrayListOf(
                    ParentMovie(ID_RECENT_VIEWED, RECENT_VIEWED),
                    ParentMovie(ID_UP_COMMING, UP_COMMING),
                    ParentMovie(ID_POPULAR, POPULAR),
                    ParentMovie(ID_TOP_RATED, TOP_RATED),
                ),
                it, lifecycle, viewLifecycleOwner
            )
        }
        val adapterSlider = SliderAdapter()
        mLayoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        recyclerViewSlider.layoutManager = mLayoutManager
        recyclerViewSlider.adapter = adapterSlider
        recyclerViewSlider.scrollToPosition(mPosition)
        val pagerSnapHelper = PagerSnapHelper()
        pagerSnapHelper.attachToRecyclerView(recyclerViewSlider)
        circleIndicator.attachToRecyclerView(recyclerViewSlider, pagerSnapHelper)
        adapterSlider.registerAdapterDataObserver(circleIndicator.adapterDataObserver)
        recyclerViewSlider.smoothScrollBy(5, 0)
        mViewModelMovie.getMovie1()
        mViewModelMovie.getMovieSlider().observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            adapterSlider.submitList(it)
        })
    }

    override fun onResume() {
        super.onResume()
        runAutoScrollBanner()
    }

    override fun onPause() {
        super.onPause()
        stopAutoScrollBanner()
    }

    override fun onDestroy() {
        super.onDestroy()
        mDisposable.clear()
    }

    override fun clickItem(item: Movie) {
        parentFragmentManager.beginTransaction()
            .replace(R.id.fragmentContainer, DetailMovieFragment.newInstance(item))
            .addToBackStack(null)
            .commit()
    }

    override fun clickItem(item: Int) {
        parentFragmentManager.beginTransaction()
            .replace(R.id.fragmentContainer,MoreMovieFragment.newInstance(item))
            .addToBackStack(null)
            .commit()
    }

    private fun runAutoScrollBanner() {
        mTimer = Timer()
        mTimerTask = object : TimerTask() {
            override fun run() {
                if (mPosition == mAdapter.itemCount) {
                    mPosition = 0
                    recyclerViewSlider.smoothScrollToPosition(mPosition)
                } else {
                    mPosition++
                    recyclerViewSlider.smoothScrollToPosition(mPosition)
                }
            }
        }
        mTimer.schedule(mTimerTask, 2000, 2000)
    }

    private fun stopAutoScrollBanner() {
        mTimer.cancel()
        mTimerTask.cancel()
        mPosition = mLayoutManager.findFirstCompletelyVisibleItemPosition()
    }

    companion object {
        var mPosition: Int = 0
        val mDisposable = CompositeDisposable()
        lateinit var mViewModelMovie: ListMovieViewModel
        fun newInstance() = ListMovieFragment()
    }
}
