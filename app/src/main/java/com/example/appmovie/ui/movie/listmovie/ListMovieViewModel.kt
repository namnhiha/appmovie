package com.example.appmovie.ui.movie.listmovie

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.rxjava2.cachedIn
import com.example.appmovie.data.model.Movie
import com.example.appmovie.data.model.MovieResponse
import com.example.appmovie.data.model.recents.MovieRecent
import com.example.appmovie.data.repository.MovieRepository
import com.example.appmovie.utils.Resource
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ListMovieViewModel @Inject constructor(private var repository: MovieRepository) :
    ViewModel() {
    private var mCompositeDisposable = CompositeDisposable()
    private var mCurrentResult: Flowable<PagingData<Movie>>? = null
    private var mMovieRecent = MutableLiveData<Resource<MutableList<MovieRecent>>>()
    private var mMovieSlider = MutableLiveData<MutableList<Movie>>()
    private var mRequestMovieMutiple = mutableListOf<Single<MovieResponse>>()
    private var mListCurrentResult = MutableLiveData<MutableList<MovieResponse>?>()
    private var mBooleanFavorite = MutableLiveData<Boolean>()

    init {
        fetchMovieRecent()
        fetchMovieZip()
    }

    @SuppressLint("CheckResult")
    fun getMovie1() {
        mCompositeDisposable.add(
            repository.getMoive1().map {
                val list = mutableListOf<Movie>()
                it.movieList.let { it ->
                    if (it.size > 3) {
                        list.addAll(it.subList(0, 3))
                    }
                }
                list
            }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        mMovieSlider.postValue(it)
                    },
                    {
                        Log.i("TAG", it.message.toString())
                    }
                )
        )
    }

    fun getMovie(): Flowable<PagingData<Movie>> {
        val newResult: Flowable<PagingData<Movie>> =
            repository.getMovie()
                .map { element -> element.mapSync { item -> item.copy(voteAverage = (item.voteAverage * 0.5).toFloat()) } }
                .cachedIn(viewModelScope)
        mCurrentResult = newResult
        return newResult
    }

    fun getUpcomingMovie(): Flowable<PagingData<Movie>> {
        val newResult: Flowable<PagingData<Movie>> =
            repository.getUpcomingMovie()
                .map { element -> element.mapSync { item -> item.copy(voteAverage = (item.voteAverage * 0.5).toFloat()) } }
                .cachedIn(viewModelScope)
        mCurrentResult = newResult
        return newResult
    }

    fun getTopRatedMovie(): Flowable<PagingData<Movie>> {
        val newResult: Flowable<PagingData<Movie>> =
            repository.getTopRatedMovie()
                .map { element -> element.mapSync { item -> item.copy(voteAverage = (item.voteAverage * 0.5).toFloat()) } }
                .cachedIn(viewModelScope)
        mCurrentResult = newResult
        return newResult
    }

    private fun fetchMovieRecent() {
        mCompositeDisposable.add(
            repository.getAllMovieRecent().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        mMovieRecent.postValue(Resource.success(it))
                    },
                    {
                        mMovieRecent.postValue(Resource.error("Something Error", null))
                    }
                )
        )
    }

    @SuppressLint("CheckResult")
    fun fetchMovieZip() {
        val movieZip = repository.getMovieZip()
        val ratedZip = repository.getTopRatedMovieZip()
        val upComingZip = repository.getUpcomingMovieZip()

        Single.zip(
            movieZip,
            ratedZip,
            upComingZip,
            { t1, t2, t3 ->
                var a = mutableListOf<Movie>()
                var b = mutableListOf<Movie>()
                var c = mutableListOf<Movie>()
                val movieList1 = t1.movieList.map {
                    it.copy(voteAverage = it.voteAverage * 0.5.toFloat())
                }
                if (movieList1.size > 5) {
                    a = movieList1.subList(0, 5) as MutableList<Movie>
                } else {
                    a = movieList1 as MutableList<Movie>
                }
                val movieResponse1 = t1.copy(movieList = a)
                val movieList2 = t2.movieList.map {
                    it.copy(voteAverage = it.voteAverage * 0.5.toFloat())
                }
                if (movieList2.size > 5) {
                    b = movieList2.subList(0, 4) as MutableList<Movie>
                } else {
                    b = movieList2 as MutableList<Movie>
                }
                val movieResponse2 = t2.copy(movieList = b)
                val movieList3 = t3.movieList.map {
                    it.copy(voteAverage = it.voteAverage * 0.5.toFloat())
                }
                if (movieList3.size > 5) {
                    c = movieList3.subList(0, 4) as MutableList<Movie>
                } else {
                    c = movieList3 as MutableList<Movie>
                }
                val movieResponse3 = t3.copy(movieList = c)
                mutableListOf(movieResponse1, movieResponse2, movieResponse3)
            })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    mListCurrentResult.postValue(it)
                },
                {
                    Log.i("TAG", "Da vo day")
                    mListCurrentResult.postValue(null)
                }
            )
    }

    fun getMovieFavorite(id: Int) {
        mCompositeDisposable.add(
            repository.getMovieFavoriteId(id).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        Log.i("TAG", "ÁDSADsadsdsa" + it)
                        mBooleanFavorite.postValue(it)
                    },
                    {
                        mBooleanFavorite.postValue(false)
                    }
                )
        )
    }

    fun getMovieFavorite(): LiveData<Boolean> {
        return mBooleanFavorite
    }

    fun getFetchMovieZip(): LiveData<MutableList<MovieResponse>?> {
        return mListCurrentResult
    }

    fun getMovieSlider(): LiveData<MutableList<Movie>> {
        return mMovieSlider
    }

    fun getMovieRecent(): LiveData<Resource<MutableList<MovieRecent>>> {
        return mMovieRecent
    }

    override fun onCleared() {
        super.onCleared()
        mCompositeDisposable.clear()
    }
}
