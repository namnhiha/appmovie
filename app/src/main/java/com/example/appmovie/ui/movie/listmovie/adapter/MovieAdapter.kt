package com.example.appmovie.ui.movie.listmovie.adapter

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.appmovie.R
import com.example.appmovie.data.model.Movie
import com.example.appmovie.ui.movie.listmovie.ItemClick
import com.example.appmovie.ui.movie.listmovie.ListMovieFragment
import com.example.appmovie.utils.Config
import kotlinx.android.synthetic.main.movie_horizontal_item.view.*
import java.util.concurrent.Executors

class MovieAdapter : ListAdapter<Movie, MovieAdapter.MovieHolder>(
    AsyncDifferConfig.Builder(COMPARATOR)
        .setBackgroundThreadExecutor(Executors.newSingleThreadExecutor())
        .build()
) {
    private var itemClick: ItemClick<Movie>? = null
    private var mViewLifecycleOwner: LifecycleOwner? = null

    fun onClick(itemClick: ItemClick<Movie>) {
        this.itemClick = itemClick
    }

    fun setViewLifeCyclerOwner(viewLifecyclerOwner: LifecycleOwner) {
        this.mViewLifecycleOwner = viewLifecyclerOwner
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.movie_horizontal_item, parent, false)
        return MovieHolder(view)
    }

    override fun onBindViewHolder(holder: MovieHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class MovieHolder(view: View) : RecyclerView.ViewHolder(view) {
        @SuppressLint("SetTextI18n")
        fun bind(movie: Movie) {
            ListMovieFragment.mViewModelMovie.getMovieFavorite(movie.id)
            mViewLifecycleOwner?.let {
                ListMovieFragment.mViewModelMovie.getMovieFavorite().observe(
                    it,
                    Observer {
                        Log.i("TAG", "aaaaaa" + it.toString())
                        itemView.apply {
                            Log.i("TAG", "AWAY" + it)
                            if (it == true) {
                                imageViewFavorite.setBackgroundResource(R.drawable.ic_favorite_yellow)
                                imageViewBookMark.setBackgroundResource(R.drawable.ic_book_mark_yellow)
                            } else {
                                imageViewFavorite.setBackgroundResource(R.drawable.ic_save_favorite)
                                imageViewBookMark.setBackgroundResource(R.drawable.bookmark)
                            }
                        }
                    })
            }
            val moviePosterURL = Config.POSTER_BASE_URL + movie.posterPath
            itemView.apply {
                textViewNameMovie.text = movie.title.toString()
                Glide.with(this).load(moviePosterURL).into(imageViewPoster)
                textViewVoteAverage.text = movie.voteAverage.toString()
                textViewVoteCount.text = "(${movie.voteCount})"
            }
            itemView.setOnClickListener {
                itemClick?.clickItem(
                    Movie(
                        movie.id,
                        movie.posterPath,
                        movie.releaseDate,
                        movie.title,
                        movie.voteAverage,
                        movie.voteCount,
                        movie.overView
                    )
                )
            }
        }
    }

    override fun submitList(list: MutableList<Movie>?) {
        super.submitList(ArrayList<Movie>(list ?: listOf()))
    }

    companion object {
        private val COMPARATOR = object : DiffUtil.ItemCallback<Movie>() {
            override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean {
                return oldItem.id == newItem.id
            }
        }
    }
}
