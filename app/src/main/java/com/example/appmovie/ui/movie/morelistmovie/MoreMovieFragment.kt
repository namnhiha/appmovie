package com.example.appmovie.ui.movie.morelistmovie

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.appmovie.R
import com.example.appmovie.ui.movie.adapter.MoviesRxAdapter
import com.example.appmovie.ui.movie.adapter.VerticalSpaceItemDecoration
import com.example.appmovie.utils.ID_POPULAR
import com.example.appmovie.utils.ID_TOP_RATED
import com.example.appmovie.utils.ID_UP_COMMING
import com.example.appmovie.utils.Status
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_more_movie.*
import javax.inject.Inject

class MoreMovieFragment : DaggerFragment(R.layout.fragment_more_movie) {
    @Inject
    lateinit var mViewModelFactory: ViewModelProvider.Factory
    private var action: Int? = null
    lateinit var mMoreMovieViewModel: MoreMovieViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            action = it.getInt(ID_MORE_MOVIE)
        }
        mMoreMovieViewModel =
            ViewModelProvider(this, mViewModelFactory).get(MoreMovieViewModel::class.java)
        when (action) {
            ID_POPULAR -> {
                mMoreMovieViewModel.getMovie()
            }
            ID_UP_COMMING -> {
                mMoreMovieViewModel.getTopRatedMovie()
            }
            ID_TOP_RATED -> {
                mMoreMovieViewModel.getTopRatedMovie()
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val moreMovieAdapter = MoviesRxAdapter()
        val verticalSpaceItemDecoration = VerticalSpaceItemDecoration(20)
        recyclerViewMoreMovie.adapter = moreMovieAdapter
        recyclerViewMoreMovie.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        recyclerViewMoreMovie.addItemDecoration(verticalSpaceItemDecoration)
        mMoreMovieViewModel.getCurrentMoreMovie().observe(viewLifecycleOwner, Observer {
            when (it.status) {
                Status.SUCCESS -> {
                    Log.i("TAG", "Da toi day")
                    it.data?.let { it1 -> moreMovieAdapter.submitData(lifecycle, it1) }
                }
                Status.RUNNING -> {

                }
                Status.FAILED -> {
                }
            }
        })
    }

    companion object {
        const val ID_MORE_MOVIE = "ID_MORE_MOVIE"
        fun newInstance(item: Int) = MoreMovieFragment().apply {
            arguments = Bundle().apply {
                putInt(ID_MORE_MOVIE, item)
            }
        }
    }
}
