package com.example.appmovie.ui.movie.morelistmovie

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.rxjava2.cachedIn
import com.example.appmovie.data.model.Movie
import com.example.appmovie.data.repository.MovieRepository
import com.example.appmovie.utils.Resource
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class MoreMovieViewModel @Inject constructor(private var repository: MovieRepository) :
    ViewModel() {
    private var mCompositeDisposable = CompositeDisposable()
    private val mMovieMoreMovie = MutableLiveData<Resource<PagingData<Movie>>>()

    fun getMovie() {
        mCompositeDisposable.add(
            repository.getMovie()
                .map { it.mapSync { item -> item.copy(voteAverage = (Math.round(item.voteAverage * 5 / 10)).toFloat()) } }
                .cachedIn(viewModelScope).subscribe(
                    {
                        mMovieMoreMovie.postValue(Resource.success(it))
                    },
                    {
                        mMovieMoreMovie.postValue(Resource.error("Something Error", null))
                    }
                )
        )
    }

    fun getUpcomingMovie() {
        mCompositeDisposable.add(
            repository.getUpcomingMovie()
                .map { it.mapSync { item -> item.copy(voteAverage = (Math.round(item.voteAverage * 5 / 10)).toFloat()) } }
                .cachedIn(viewModelScope).subscribe(
                    {
                        mMovieMoreMovie.postValue(Resource.success(it))
                    },
                    {
                        mMovieMoreMovie.postValue(Resource.error("Something Error", null))
                    }
                )
        )
    }

    fun getTopRatedMovie() {
        mCompositeDisposable.add(
            repository.getTopRatedMovie()
                .map { it.mapSync { item -> item.copy(voteAverage = (Math.round(item.voteAverage * 5 / 10)).toFloat()) } }
                .cachedIn(viewModelScope).subscribe(
                    {
                        mMovieMoreMovie.postValue(Resource.success(it))
                    },
                    {
                        mMovieMoreMovie.postValue(Resource.error("Something Error", null))
                    }
                )
        )
    }

    fun getCurrentMoreMovie(): LiveData<Resource<PagingData<Movie>>> {
        return mMovieMoreMovie
    }

    override fun onCleared() {
        super.onCleared()
        mCompositeDisposable.clear()
    }
}
