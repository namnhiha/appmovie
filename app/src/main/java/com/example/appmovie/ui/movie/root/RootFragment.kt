package com.example.appmovie.ui.movie.root

import android.os.Bundle
import android.view.View
import androidx.viewpager2.widget.ViewPager2
import com.example.appmovie.R
import com.example.appmovie.ui.movie.adapter.HomeViewPagerAdapter
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_root.*

class RootFragment : DaggerFragment(R.layout.fragment_root) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    override fun onResume() {
        super.onResume()
        activity?.supportFragmentManager?.setFragmentResultListener(
            "requestKey",
            viewLifecycleOwner
        ) { _, bundle ->
            val result = bundle.getBoolean("bundleKey")
            if (result) bottomNavigationView.visibility = View.INVISIBLE
            else bottomNavigationView.visibility = View.VISIBLE
        }
        bottomNavigationView.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.action_home -> {
                    viewpager2ListMovie.currentItem = 0
                    true
                }
                R.id.action_favorite -> {
                    viewpager2ListMovie.currentItem = 1
                    true
                }
                R.id.action_search -> {
                    viewpager2ListMovie.currentItem = 2
                    true
                }
                R.id.action_setting -> {
                    viewpager2ListMovie.currentItem = 3
                    true
                }
                else -> true
            }
        }
        viewpager2ListMovie.registerOnPageChangeCallback(object :
            ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                when (position) {
                    0 -> {
                        bottomNavigationView.menu.findItem(R.id.action_home).isChecked = true
                    }
                    1 -> {
                        bottomNavigationView.menu.findItem(R.id.action_favorite).isChecked = true
                    }
                    2 -> {
                        bottomNavigationView.menu.findItem(R.id.action_search).isChecked = true
                    }
                    3 -> {
                        bottomNavigationView.menu.findItem(R.id.action_setting).isChecked = true
                    }
                }
            }
        })
    }

    private fun initView() {
        viewpager2ListMovie.adapter = activity?.let { HomeViewPagerAdapter(it) }
        viewpager2ListMovie.offscreenPageLimit = 3
    }

    fun onBackPressed(): Boolean {
        when (viewpager2ListMovie.currentItem) {
            0 -> {
                return false
            }
            1 -> {
                viewpager2ListMovie.currentItem = 0
                return true
            }
            2 -> {
                viewpager2ListMovie.currentItem = 0
                return true
            }
            else -> return false
        }
    }

    companion object {
        fun newInstance() = RootFragment()
    }
}
