package com.example.appmovie.ui.movie.searchmovie

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.widget.SearchView
import com.example.appmovie.R
import com.example.appmovie.ui.movie.searchmovie.listsearch.ListSearchFragment
import com.example.appmovie.ui.movie.searchmovie.recent.RecentSearchFragment
import dagger.android.support.DaggerFragment
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_search_movie.*

class SearchMovieFragment : DaggerFragment(R.layout.fragment_search_movie),
    SearchView.OnQueryTextListener {
    private var default = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
        Log.i("TAG","onStart")
    }

    override fun onResume() {
        super.onResume()
        searchViewMovie.setOnQueryTextListener(this)
        if (!default) {
            parentFragmentManager.beginTransaction()
                .add(R.id.frameContainerSearch, RecentSearchFragment.newInstance())
                .commit()
            default = true
        }
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        Log.i("TAG", "Query" + query.toString())
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        Log.i("TAG", "Change" + newText.toString())
        newText?.let {
            if (it.isEmpty()) {
                parentFragmentManager.beginTransaction()
                    .replace(R.id.frameContainerSearch, RecentSearchFragment.newInstance())
                    .commit()
            } else
                parentFragmentManager.beginTransaction()
                    .replace(
                        R.id.frameContainerSearch,
                        ListSearchFragment.newInstance(newText.toString())
                    )
                    .commit()
        }
        return false
    }

    override fun onDestroy() {
        super.onDestroy()
        mDisposable.clear()
    }

    companion object {
        val mDisposable = CompositeDisposable()
        fun newInstance() = SearchMovieFragment()
    }
}
