package com.example.appmovie.ui.movie.searchmovie.listsearch

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.appmovie.R
import com.example.appmovie.data.model.Movie
import com.example.appmovie.ui.movie.adapter.SearchMovieRxAdapter
import com.example.appmovie.ui.movie.detailmovie.DetailMovieFragment
import com.example.appmovie.ui.movie.favoritemovie.adapter.VerticalSpaceItemDecoration
import com.example.appmovie.ui.movie.listmovie.ItemClick
import dagger.android.support.DaggerFragment
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_list_search_movie.*
import javax.inject.Inject

class ListSearchFragment : DaggerFragment(R.layout.fragment_list_search_movie), ItemClick<Movie> {
    @Inject
    lateinit var mViewModelFactory: ViewModelProvider.Factory
    lateinit var mAdapter: SearchMovieRxAdapter
    private var mQuery: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            mQuery = it.getString(ID_LIST_SEARCH_FRAGMENT)
        }
        mListSearchMovieViewModel =
            ViewModelProvider(this, mViewModelFactory).get(ListSearchMovieViewModel::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mAdapter = SearchMovieRxAdapter()
        mAdapter.onClick(this)
        val layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        recyclerViewSearchMovie.layoutManager = layoutManager
        recyclerViewSearchMovie.adapter = mAdapter
        val verticalSpaceItemDecoration=VerticalSpaceItemDecoration(20)
        recyclerViewSearchMovie.addItemDecoration(verticalSpaceItemDecoration)
    }

    override fun onResume() {
        super.onResume()
        mQuery?.let { mListSearchMovieViewModel.getMovie(it) }
        mListSearchMovieViewModel.getMovieSearch().observe(this, Observer {
            it.data?.let { it1 -> mAdapter.submitData(lifecycle, it1) }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        mDisposable.clear()
    }

    override fun clickItem(item: Movie) {
        parentFragmentManager.beginTransaction()
            .replace(R.id.fragmentContainer, DetailMovieFragment.newInstance(item))
            .addToBackStack(null)
            .commit()
    }

    companion object {
        val mDisposable = CompositeDisposable()
        lateinit var mListSearchMovieViewModel: ListSearchMovieViewModel
        const val ID_LIST_SEARCH_FRAGMENT = "ID_LIST_SEARCH_FRAGMENT"
        fun newInstance(query: String) = ListSearchFragment().apply {
            arguments = Bundle().apply {
                putString(ID_LIST_SEARCH_FRAGMENT, query)
            }
        }
    }
}
