package com.example.appmovie.ui.movie.searchmovie.listsearch

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.rxjava2.cachedIn
import com.example.appmovie.data.model.Movie
import com.example.appmovie.data.repository.SearchRepository
import com.example.appmovie.utils.Resource
import io.reactivex.Flowable
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class ListSearchMovieViewModel @Inject constructor(private var repository: SearchRepository) :
    ViewModel() {
    private var mCompositeDisposable = CompositeDisposable()
    private var currentResult: Flowable<PagingData<Movie>>? = null
    private val mMovieSearch = MutableLiveData<Resource<PagingData<Movie>>>()

    fun getMovie(query: String) {
        val newResult: Flowable<PagingData<Movie>> =
            repository.searchMovie(query).cachedIn(viewModelScope)
        currentResult = newResult
        mCompositeDisposable.add(repository.searchMovie(query).cachedIn(viewModelScope).subscribe(
            {
                mMovieSearch.postValue(Resource.success(it))
            }, {
                mMovieSearch.postValue(Resource.error("Something Error", null))
            }
        ))
    }

    fun getMovieSearch(): LiveData<Resource<PagingData<Movie>>> {
        return mMovieSearch
    }

    override fun onCleared() {
        super.onCleared()
        mCompositeDisposable.clear()
    }
}
