package com.example.appmovie.ui.movie.searchmovie.recent

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.example.appmovie.R
import com.example.appmovie.data.model.Movie
import com.example.appmovie.ui.movie.adapter.GridSpaceItemDecoration
import com.example.appmovie.ui.movie.detailmovie.DetailMovieFragment
import com.example.appmovie.ui.movie.listmovie.ItemClick
import com.example.appmovie.ui.movie.searchmovie.recent.adapter.RecentAdapter
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_recent_search_movie.*
import javax.inject.Inject

class RecentSearchFragment : DaggerFragment(R.layout.fragment_recent_search_movie),
    ItemClick<Movie> {
    @Inject
    lateinit var mViewModelFactory: ViewModelProvider.Factory
    lateinit var mRecentAdapter: RecentAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModelRecent =
            ViewModelProvider(this, mViewModelFactory).get(RecentSearchViewModel::class.java)
        mRecentAdapter = RecentAdapter()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(this)
    }

    override fun onResume() {
        super.onResume()
        textViewClear.setOnClickListener {
            mViewModelRecent.deleteMovieRecent()
        }
    }

    fun initView(itemClick: ItemClick<Movie>) {
        val gridLayoutManager = GridLayoutManager(activity, 2, GridLayoutManager.VERTICAL, false)
        recyclerViewMovieRecent.layoutManager = gridLayoutManager
        recyclerViewMovieRecent.adapter = mRecentAdapter
        val gridSpaceItemDecoration = GridSpaceItemDecoration(5)
        recyclerViewMovieRecent.addItemDecoration(gridSpaceItemDecoration)
        mRecentAdapter.setClick(itemClick)
        mViewModelRecent.getMovieRecent().observe(viewLifecycleOwner, Observer {
            mRecentAdapter.submitList(it.data)
        })
    }

    override fun clickItem(item: Movie) {
        parentFragmentManager.beginTransaction()
            .replace(R.id.fragmentContainer, DetailMovieFragment.newInstance(item))
            .addToBackStack(null)
            .commit()
    }

    companion object {
        lateinit var mViewModelRecent: RecentSearchViewModel
        fun newInstance() = RecentSearchFragment()
    }
}
