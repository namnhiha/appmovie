package com.example.appmovie.ui.movie.searchmovie.recent

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.appmovie.data.model.recents.MovieRecent
import com.example.appmovie.data.repository.RecentSearchRepository
import com.example.appmovie.utils.Resource
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class RecentSearchViewModel @Inject constructor(private val repository: RecentSearchRepository) :
    ViewModel() {
    private var mCompositeDisposable = CompositeDisposable()
    private var mMovieRecent = MutableLiveData<Resource<MutableList<MovieRecent>>>()

    init {
        fetchMovieRecent()
    }

    private fun fetchMovieRecent() {
        mCompositeDisposable.add(
            repository.getAllMovieRecent().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        mMovieRecent.postValue(Resource.success(it))
                    },
                    {
                        mMovieRecent.postValue(Resource.error("Something Error", null))
                    }
                )
        )
    }

    fun deleteMovieRecent() {
        Single.fromCallable {
            repository.deleteAllMovieRecent()
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe()
    }

    fun getMovieRecent(): LiveData<Resource<MutableList<MovieRecent>>> {
        return mMovieRecent
    }

    override fun onCleared() {
        super.onCleared()
        mCompositeDisposable.clear()
    }
}
