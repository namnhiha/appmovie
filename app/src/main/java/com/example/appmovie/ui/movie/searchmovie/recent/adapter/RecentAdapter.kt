package com.example.appmovie.ui.movie.searchmovie.recent.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.appmovie.R
import com.example.appmovie.data.model.Movie
import com.example.appmovie.data.model.recents.MovieRecent
import com.example.appmovie.ui.movie.listmovie.ItemClick
import com.example.appmovie.utils.Config
import kotlinx.android.synthetic.main.movie_horizontal_item_recent.view.*
import java.util.concurrent.Executors

class RecentAdapter : ListAdapter<MovieRecent, RecentAdapter.RecentHolder>(
    AsyncDifferConfig.Builder(COMPARATOR)
        .setBackgroundThreadExecutor(Executors.newSingleThreadExecutor())
        .build()
) {
    private var mClickItem: ItemClick<Movie>? = null

    fun setClick(item: ItemClick<Movie>) {
        mClickItem = item
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecentHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.movie_horizontal_item_recent, parent, false)
        return RecentHolder(view)
    }

    override fun onBindViewHolder(holder: RecentHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class RecentHolder(view: View) : RecyclerView.ViewHolder(view) {
        @SuppressLint("SetTextI18n")
        fun bind(movie: MovieRecent) {
            val moviePosterURL = Config.BACKDROP_BASE_URL + movie.posterPath
            itemView.apply {
                Glide.with(this).load(moviePosterURL).into(imageViewPoster)
                textViewNameMovie.text = movie.title
            }
            itemView.setOnClickListener {
                    mClickItem?.clickItem(
                        Movie(
                            movie.id,
                            movie.posterPath,
                            movie.releaseDate,
                            movie.title,
                            movie.voteAverage,
                            movie.voteCount,
                            movie.overView
                        )
                    )
            }
        }
    }

    override fun submitList(list: MutableList<MovieRecent>?) {
        super.submitList(ArrayList<MovieRecent>(list ?: listOf()))
    }

    companion object {
        private val COMPARATOR = object : DiffUtil.ItemCallback<MovieRecent>() {
            override fun areItemsTheSame(oldItem: MovieRecent, newItem: MovieRecent): Boolean {
                return  oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: MovieRecent, newItem: MovieRecent): Boolean {
                return oldItem.id==newItem.id
            }
        }
    }
}
