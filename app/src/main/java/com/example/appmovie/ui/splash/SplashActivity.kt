package com.example.appmovie.ui.splash

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import com.example.appmovie.ui.movie.MainActivity
import dagger.android.support.DaggerAppCompatActivity

@SuppressLint("CustomSplashScreen")
class SplashActivity : DaggerAppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}
