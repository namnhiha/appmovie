package com.example.appmovie.utils

object Config {
    const val API_KEY = "1cf151ff23247b3fe3558107667a395f"
    const val BASE_URL = "https://api.themoviedb.org/3/"
    const val POSTER_BASE_URL = "https://image.tmdb.org/t/p/w342"
    const val BACKDROP_BASE_URL = "https://image.tmdb.org/t/p/w1280"
}
