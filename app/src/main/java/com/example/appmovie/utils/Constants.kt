package com.example.appmovie.utils

const val STARTING_PAGE = 1
const val ID_RECENT_VIEWED = 1
const val ID_UP_COMMING = 2
const val ID_POPULAR = 3
const val ID_TOP_RATED = 4
const val ID_SIMILAR_MOVIE = 1
const val ID_RECOMENDATION_MOVIE = 2
const val RECENT_VIEWED = "RecentViewed"
const val UP_COMMING = "Up comming"
const val POPULAR = "Popular"
const val TOP_RATED = "Top Rated"
const val SIMILAR_MOVIE = "Similar Movies"
const val RECOMENDATION_MOVIE = "Recomendation Movies"
